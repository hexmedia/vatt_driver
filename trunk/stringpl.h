#ifndef _STRINGPL_H_
#define _STRINGPL_H_

extern int strcmp_pl(const char *, const char *);
extern int strcmp_nsym_pl(const char *, const char *);
extern int strncmp_pl(const char *, const char *, int);
extern int strncmp_nsym_pl(const char *, const char *, int);

#endif /* _STRINGPL_H_ */
