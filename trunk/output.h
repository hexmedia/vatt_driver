#ifndef _OUTPUT_H_
#define _OUTPUT_H_

void parse_output(char *, struct object *);

#ifdef USE_ANSI_COLORS
char   *ansi_clear_color_string(const char *);
size_t  ansi_strlen(const char *);
#endif /*USE_ANSI_COLORS*/

#ifdef USE_DIACRITICAL_MARKS
void  process_player_input(char *, struct object *);
char *plain_string_pl(const char *);
#endif /*USE_DIACRITICAL_MARKS*/

#endif /*_OUTPUT_H_*/
