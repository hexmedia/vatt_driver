
#include "config.h"

#ifdef USE_DICTIONARY

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <mysql/mysql.h>

#include "lint.h"
#include "simulate.h"
#include "dictionary.h"
#include "mysql.h"
#include "mstring.h"
#include "interpret.h"
#include "object.h"
#include "inline_eqs.h"

struct dictionary *dict = NULL;
struct dictionary *last_dict = NULL;
   

/**
 * Funkcja alokuj帷a s這wo w s這wniku.
 */
static struct dict_word *
allocate_dict_word(void)
{
    struct dict_word *w;
    
    w = (struct dict_word *)malloc(sizeof(struct dict_word));
 
    w->mn_mia = NULL;
    w->mn_cel = NULL;
    w->mn_dop = NULL;
    w->mn_bie = NULL;
    w->mn_nar = NULL;
    w->mn_mie = NULL;
    
    return w;
}

/**
 * Funkcja zwalniaj帷a s這wo.
 */
static void
free_dict_word(struct dict_word *w)
{
    free_mstring(w->lp_mia);
    free_mstring(w->lp_dop);
    free_mstring(w->lp_cel);
    free_mstring(w->lp_bie);
    free_mstring(w->lp_nar);
    free_mstring(w->lp_mie);
    
    if (w->mn)
    {
        free_mstring(w->mn_mia);
        free_mstring(w->mn_dop);
        free_mstring(w->mn_cel);
        free_mstring(w->mn_bie);
        free_mstring(w->mn_nar);
        free_mstring(w->mn_mie);
    }
}

/**
 * Alokuje nowe s這wo do s這wnika.
 */
struct dictionary *
allocate_next_dict(struct dict_word *w)
{    
    struct dictionary *d;

    d = (struct dictionary *)malloc(sizeof(struct dictionary));

    if (!d)
    {
        error("Out of memory!\n");
        return NULL;
    }

    d->next = NULL;
    d->prev = last_dict;
    d->word = w;
    
    if (!dict)
        dict = d;
    
    if (last_dict)
    {
        d->prev = last_dict->prev;
        last_dict->next = d;
    }
    else
        d->prev = NULL;
    
    last_dict = d;
    
    return d;
}


/**
 * Funkcja indeksuj帷a s這wnik.
 */
void
dictionary_index(void)
{
    int conn_ident;
    struct mysql_handle *h;
    
    conn_ident = lpc_mysql_connect(DICTIONARY_MYSQL_USER, DICTIONARY_MYSQL_PASSWD, DICTIONARY_MYSQL_DBNAME, DICTIONARY_MYSQL_DBHOST, DICTIONARY_MYSQL_DBPORT);
    
    if (!conn_ident)
        error("Nie uda這 si� zainicjowa� po陰czenia z mysql'em.");
    
    h = get_mysql_handle_by_ident(conn_ident);
    
    if (!mysql_real_query(h->handler, "SELECT `rodzaj`, `mianownik`, `dopelniacz`, `celownik`, `biernik`, `narzednik`, `miejscownik`, `mn_mianownik`, `mn_dopelniacz`, `mn_celownik`, `mn_biernik`, `mn_narzednik`, `mn_miejscownik` FROM `slownik`", 204))
    {
        h->result = mysql_use_result(h->handler);
        
        while ((h->row = mysql_fetch_row(h->result)) && h->row)
        {
            if (h->row[1] && h->row[2] && h->row[3] && h->row[4] && h->row[5] && h->row[6])
            {
                struct dict_word *w;
                
                w = allocate_dict_word();
                
                w->lp_mia = make_mstring((const char *)h->row[1]);
                w->lp_dop = make_mstring((const char *)h->row[2]);
                w->lp_cel = make_mstring((const char *)h->row[3]);
                w->lp_bie = make_mstring((const char *)h->row[4]);
                w->lp_nar = make_mstring((const char *)h->row[5]);
                w->lp_mie = make_mstring((const char *)h->row[6]);
                
                if (h->row[7] && h->row[8] && h->row[9] && h->row[10] && h->row[11] && h->row[12])
                {
                    w->mn = 0;
                    w->mn_mia = make_mstring((const char *)h->row[7]);
                    w->mn_dop = make_mstring((const char *)h->row[8]);
                    w->mn_cel = make_mstring((const char *)h->row[9]);
                    w->mn_bie = make_mstring((const char *)h->row[10]);
                    w->mn_nar = make_mstring((const char *)h->row[11]);
                    w->mn_mie = make_mstring((const char *)h->row[12]);
                }
                else
                {
                    w->mn = 1;
                }
                
                w->rodz = (long long)strtoll(h->row[0], NULL, 10);
                
                (void)allocate_next_dict(w);
            }
        }
        
        mysql_free_result(h->result);
    }
    
    mysql_close(h->handler);
}        

/**
 * Funkcja czyszcz帷a s這wnik i zwalniaj帷a pami耩.
 */
void
dictionary_kill(void)
{   
    while(dict != NULL)
    {
        if (dict->word != NULL)
            free_dict_word(dict->word);
        
        dict = dict->next;
        
        free(dict->prev);
    }
}

/**
 * Funkcja reindeksuj帷a s這wnik.
 */
void
dictionary_reindex(void) 
{
    dictionary_kill();
    dictionary_index();
}
/**
 * Funkcja wyszukujaca i zwracaj帷a czy s這wo kt鏎ego szukamy istnieje w s這wniku.
 *
 * @param word S這wo kt鏎ego szukamy.
 *
 * @return 1 - s這wo istnieje, 2 - s這wo istnieje, i jest to forma mnoga.
 */
short
dictionary_check(char *word)
{
    struct dictionary *d;
    
    if (dict == NULL)
        dictionary_index();
    
    d = dict;
    
    do 
    {
        d = d->next;
        
        if (d->word)
        {
            if (strcmp_nsym_pl(word, d->word->lp_mia)) 
                return 1;
            else if (!d->word->mn && strcmp_nsym_pl(word, d->word->mn_mia)) 
                return 2;
        }
    } while (d->next != NULL);
    
    return 0;
}

/**
 * Funkcja wyszukuj帷a i zwracaj帷a pe軟� odmian� s這wa.
 *
 * @param word S這wo kt鏎ego szukamy.
 */
struct dict_wordr *
dictionary_search(char *word) 
{
    short flag;
    struct dictionary *d;
    struct dict_wordr *dr;
    
    if (dict == NULL)
        dictionary_index();
    
    d = dict;
    
    do 
    {
        d = d->next;
        
        if (d->word)
        {
            if (strcmp_nsym_pl(word, d->word->lp_mia) == 0)
                flag = 0;
            else if (!d->word->mn && strcmp_nsym_pl(word, d->word->mn_mia) == 0)
                flag = 1;
            else
                continue;
                
            dr = (struct dict_wordr *)malloc(sizeof(struct dict_wordr));
                
            dr->word = d->word;
            dr->mn = flag;
                
            return dr;
        }
    } while (d->next != NULL);
        
    return NULL;
}

#endif /*USE_DICTIONARY*/
