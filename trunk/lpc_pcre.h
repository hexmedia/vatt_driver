#include "config.h"
#include <pcre.h>

#define OVECTOR_SIZE        300

struct lpc_preg {
	pcre 		*pce;
	char 		*regex;
	int  		 group_count;
	pcre_extra	*extra;
};

extern struct vector 		*preg_match(struct lpc_preg *, const char *, char *, int *);
extern struct mapping       *preg_match_mapping(struct lpc_preg *, const char *, char *, int *);
extern char                 *preg_replace(struct lpc_preg *, const char *, char *, struct mapping *,  int *);
extern struct lpc_preg 	    *preg_compile(struct lpc_preg *);
extern struct lpc_preg 	    *preg_alloc(void); 
extern void 				 preg_free(struct lpc_preg *);
