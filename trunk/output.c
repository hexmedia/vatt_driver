
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>

#ifdef USE_DIACRITICAL_MARKS
#include <iconv.h>
#include <errno.h>
#endif /*USE_DIACRITICAL_MARKS*/

#include "config.h"
#include "lint.h"
#include "interpret.h"
#include "mstring.h"
#include "output.h"
#include "simulate.h"
#include "comm.h"
#include "object.h"
#include "comm1.h"

#ifdef USE_DIACRITICAL_MARKS
/**
 * Funkcja konwertuj�ca mi�dzy kodowaniami.
 * Do konwertowania wykorzystywany jest algorytm iconv
 *
 * @param cp - string do skonwerowania
 * @param from - kodowanie z kt�rego mamy konwertowa�
 * @param to - kodowanie do kt�rego konwertujemy.
 */
void
convert_encoding(char *cp, const char *from, const char *to)
{
    char *inbuff, *outbuff, *outbuffi;
    size_t inbuff_s, outbuff_s;
    iconv_t  cd;
    
    inbuff_s = strlen(cp);
    outbuff_s = inbuff_s * 2;
    
	if (strcmp(from, "utf-8") == 0) {
	    outbuff = (char *)malloc(sizeof(char) * (outbuff_s + 1) * 8);
	} else {
		outbuff = (char *)malloc(sizeof(char) * (outbuff_s + 1));
	}
	
    inbuff = cp;
    outbuffi = outbuff;
    
    cd = iconv_open(to, from);
    
    if (cd == (iconv_t) -1)
    {
        if (errno == EINVAL)
            error("Konwersja z kodowania `%s` do kodowania `%s` nie jest mo�liwa.\n", from, to);
        
        error("Inicializacja konwersji z kodowania `%s` do kodowania `%s` nie powiod�a si�.\n", from, to);
    }
    
    if (iconv(cd, &inbuff, &inbuff_s, &outbuffi, &outbuff_s) == -1) 
        error("Konwersja z kodowania `%s` do kodowania `%s` nie powiod�a si�.\n", from, to);
    
    *outbuffi = '\0';
    
    strcpy(cp, outbuff);
    
    free(outbuff);
    
    iconv_close(cd);
}

/**
 * Funkcja czy�ci stringa w iso-8859-2 z polskich znak�w, zat�puj�c je
 * ich bezogonkowymi odpowiednikami.
 *
 * @param str wska�nik do stringa kt�ry nale�y wyczy�ci�
 */
void
plain_string_pl_lv(char *str)
{
    while (*str != '\0')
    {
        switch (*str) 
        {
            case (char)177: *str = 'a'; break;
            case (char)230: *str = 'c'; break;
            case (char)234: *str = 'e'; break;
            case (char)179: *str = 'l'; break;
            case (char)241: *str = 'n'; break;
            case (char)243: *str = 'o'; break;
            case (char)182: *str = 's'; break;
            case (char)191:
            case (char)188: *str = 'z'; break;
            case (char)161: *str = 'A'; break;
            case (char)198: *str = 'C'; break;
            case (char)202: *str = 'E'; break;
            case (char)163: *str = 'L'; break;
            case (char)209: *str = 'N'; break;
            case (char)211: *str = 'O'; break;
            case (char)166: *str = 'S'; break;
            case (char)175:
            case (char)172: *str = 'Z'; break;
        }
        ++str;
    }
}


/**
* Funkcja czy�ci stringa w iso-8859-2 z polskich znak�w, zat�puj�c je
* ich bezogonkowymi odpowiednikami i zwraca wska�nik do stringa bez ogonk�w.
*
* @param str wska�nik do stringa kt�ry nale�y wyczy�ci�
*
* @return wska�nik do stringa bez ogonk�w.
*/
char *
plain_string_pl(const char *str)
{
    char *ret;
    
    ret = (char *)malloc(sizeof(char) * (1 + strlen(str)));
    
    strcpy(ret, str);
    
    plain_string_pl_lv(ret);
    
    return ret;
}
#endif /* USE_DIACRITICAL_MARKS */

/**
 * Funkcja parsuj�ca nowe elementy wyj�cia, takie jak kolorki, kodowania i MXP
 *
 * @param cp string wyj�cia
 * @param cg gracz dla kt�rego wyj�cie jest przeznaczone.
 */
void
parse_output(char *cp, struct object *cg)
{
//     W tym przypadku nie ma potrzeby prasowanie wi�c po prostu ko�czymy funkcje zanim si� zacznie
#if defined(USE_ANSI_COLORS) || defined(USE_DIACRITICAL_MARKS) || defined(USE_MXP)
	
    //Kodowanie robimy z iconv wi�c przed p�telk�.
#ifdef USE_DIACRITICAL_MARKS    
    switch (cg->interactive->encoding)
    {
        case 0:
            plain_string_pl_lv(cp);
            break;
        case 1:
            break;
        case 2:
        case 3:
            convert_encoding(cp, "iso-8859-2", "cp1250");
            break;
        case 4:
            convert_encoding(cp, "iso-8859-2", "utf-8");
            break;
    }
#endif /*USE_DIACRITICAL_MARKS*/
    
#if defined(USE_ANSI_COLORS) || defined(USE_MXP)
    {
        char *buff, *cpi, *buffi;
#ifdef USE_ANSI_COLORS
        short ansi_f;
#endif /*USE_ANSI_COLORS*/
        
#ifdef USE_MXP
        short mxp_f;
#endif /* USE_MXP */
        
#ifdef USE_ANSI_COLORS
        ansi_f = cg->interactive->ansi;
#endif /* USE_ANSI_COLORS */
        
#ifdef USE_MXP
        mxp_f = cg->interactive->mxp;
#endif /* USE_MXP */
        
        buff = (char *)malloc((MAX_WRITE_SOCKET_SIZE + 2) * sizeof(char));
        
        for ( cpi = cp, buffi = buff ; *cpi && *cpi != '\0' ; cpi++)
        {
#ifdef EXPAND_TABS_TO_SPACES
            if (*cpi == '\t')
            {
                int si;
                
                if (buffi + EXPAND_TABS_TO_SPACES > buff)
                    error("Too long message!\n");
                
                for (si = 0 ; si < EXPAND_TABS_TO_SPACES ; si++)
                    *buffi = ' ';buffi++;

                continue;
            }
#endif /* EXPAND_TABS_TO_SPACES */
            
            if (
#ifdef USE_ANSI_COLORS
                !ansi_f ||
#endif /* USE_ANSI_COLORS */
#ifdef USE_MXP
                !mxp_f
#else /* USE_MXP */
                0
#endif /* USE_MXP */
                )
            {
                if ( *cpi == '\x1b' )
                {
                    char *spi;
                    spi = (cpi + 1);
                    
                    if (*spi && *spi == '[')
                    {
                        while(*spi && *spi != 'm' && *spi != 'z' && *spi != '\0')
                            spi++;
                    }

#ifdef USE_ANSI_COLORS
                    if (*spi == 'm' && !ansi_f)
                    {
                        cpi = spi;
                        continue;
                    }
#endif /* USE_ANSI_COLORS */
                    
#ifdef USE_MXP
                    if (*spi == 'z' && !mxp_f)
                    {
                        cpi = spi;
                        continue;
                    }
#endif /* USE_MXP */
                        
                }
                
                *buffi = *cpi;
                buffi++;
            }
        }            
        
        if (
 #ifdef USE_ANSI_COLORS
            !ansi_f ||
#endif /* USE_ANSI_COLORS */
#ifdef USE_MXP
            !mxp_f
#else /* USE_MXP */
            0
#endif /* USE_MXP */
            )
        {
            *buffi = '\0';
            
            strcpy(cp, buff);
            
            free(buff);
        }
    }
#endif /*defined(USE_ANSI_COLORS) || defined(USE_MXP)*/
    
#endif  /* defined(USE_ANSI_COLORS) || defined(USE_DIACRITICAL_MARKS) || defined(USE_MXP) */
}

#ifdef USE_DIACRITICAL_MARKS

void
process_player_input(char *cp, struct object *cg)
{
	int i, encoding;
	unsigned char *ucp;
			
	//���������A��������
// 	char charTable[18] = {0xB1, 0xBF, 0xB3, 0xB6, 0xBC, 0xEA, 0xE6, 0xF1, 0xF3, 0xA1, 0xAC, 0xA3, 0xC6, 0xCA, 0xC6, 0xD1, 0xD3};
	
	ucp = (unsigned char *)cp;
	encoding = -1;
	
	for (i = 0; ucp[i] != '\0' ; i++)
	{
		if ((unsigned int)ucp[i] > 127) {
			
			switch ((unsigned int)ucp[i])
			{
				case 195:
					if ( (unsigned int)(ucp[i + 1]) == 178 || (unsigned int)(ucp[i + 1]) == 147) {
						encoding = 4;
						break;
					}
				case 196:
					if ( (unsigned int)(ucp[i + 1]) == 133 || (unsigned int)(ucp[i + 1]) == 153 || 
						 (unsigned int)(ucp[i + 1]) == 135 || (unsigned int)(ucp[i + 1]) == 132 ||
						 (unsigned int)(ucp[i + 1]) == 152 || (unsigned int)(ucp[i + 1]) == 134) 
					{
						encoding = 4;
						break;
					}	
				case 197:
					if ( (unsigned int)(ucp[i + 1]) == 188 || (unsigned int)(ucp[i + 1]) == 130 ||
						 (unsigned int)(ucp[i + 1]) == 155 || (unsigned int)(ucp[i + 1]) == 196 ||
						 (unsigned int)(ucp[i + 1]) == 132 || (unsigned int)(ucp[i + 1]) == 187 || 
						 (unsigned int)(ucp[i + 1]) == 129 || (unsigned int)(ucp[i + 1]) == 154 || 
						 (unsigned int)(ucp[i + 1]) == 185 || (unsigned int)(ucp[i + 1]) == 131 ) 
					{
						encoding = 4;
						break;
					}
				case 177:
				case 182:
				case 188:
				case 161:
				case 166:
				case 172:
					encoding = 1;
					break;
					
				case 185:
				case 156:
				case 159:
				case 165:
				case 140:
				case 143:
					encoding = 2;
					break;
				
				case 234:
				case 230:
				case 241:
				case 243:
				case 191:
				case 179:
				case 175:
				case 163:
				case 202:
				case 198:
				case 209:
				case 211:
					break;
					
				default:
					ucp[i] = '?';
					
					if (encoding == 4 && ucp[i + 1] != '\0') 
					{
						ucp[++i] = '?';
					}
			}
		}				
	}
	
	if (cg->interactive->encoding != encoding && encoding != -1) {
        cg->interactive->encoding = encoding;
        
		write_socket("\e[32m==> Twoje kodowanie zosta�o automatycznie przestawione na \e[0m", cg);
		switch (encoding)
		{
			case 1:
				write_socket("ISO-8859-2", cg);
				break;
			case 2:
			case 3:
				write_socket("CP-1250", cg);
				break;
			case 4:
				write_socket("UTF-8", cg);
				break;
		}
		
		write_socket("\e[0m\e32m\".\n", cg);
	}
	
	//Wykrywamy kodowanie usera.
// 	for (i = 0 ; cp[i] != '\0' ; i++) 
// 	{
// 		int bk;
// 		
// 		if ((int)cp[i] < 0) 
// 		{
// 			switch ((int)cp[i]) 
// 			{
// 				case -59:
// 				{
// 					bk = 0;
// 					switch ((int)cp[i + 1]) 
// 					{
// 						case -68:
// 						case -126:
// 						case -101:
// 						case -70:
// 						case -124:
// 						case -69:
// 						case -127:
// 						case -102:
// 						case -125:
// // 							cp[i] = 
// 							break;
// 						default:
// // 							cp
// 					}
// 						
// 					if (bk) {
// 						break;
// 					}
// 				}
// 				case -60:
// 						bk = 0;
// 						switch ((int)cp[i + 1]) {
// 							case -123:
// 							case -103:
// 							case -121:
// 							case -124:
// 						case -125:
// 							case -104:
// 							case -122:
// 								break;
// 							default:
// 								bk = 1;
// 						}
// 						if (bk) {
// 							break;
// 						}
// 					case -61:
// 						bk = 0;
// 						switch ((int)cp[i + 1]) {
// 							case 77:
// 							case 109:
// 								break;
// 							default:
// 								bk = 1;
// 						}
// 					
// 						if (bk) {
// 							break;
// 						}
// 					
// 						if (cg->interactive->encoding != 4) {
// 							write_socket("\e[32;1m==> Twoje kodowanie zosta�o automatycznie przestawione na \"UTF-8\".", cg);
// 							cg->interactive->encoding = 4;
// 						}
// 						
// 						break;
// 					
// 					case -79:
// 					case -74:
// 					case -68:
// 					case -95:
// 					case -90:
// 					case -85:
// 						if (cg->interactive->encoding != 1) {
// 							write_socket("\e[32;1m==> Twoje kodowanie zosta�o automatycznie przestawione na \"ISO-8859-2\".\e[0m\n", cg);
// 							cg->interactive->encoding = 1;
// 						}
// 						cm = 1;
// 						break;
// 
// 					case -71:
// 					case -97:
// 					case -100:
// 					case -91:
// 					case -116:
// 					case -113:
// 						charTable[0] = 0xB9;
// 						charTable[3] = 0x9C;
// 						charTable[4] = 0x9F;
// 						charTable[9] = 0xA5;
// 						charTable[12] = 0x8F;
// 						charTable[13] = 0x8C;
// 						cm = 1;
// 						if (cg->interactive->encoding != 2 && cg->interactive->encoding != 3) {
// 							cg->interactive->encoding = 2;
// 							write_socket("\e[32;1m==> Twoje kodowanie zosta�o automatycznie przestawione na \"CP-1250\".\e[0m\n", cg);
// 						}
// 						cm = 1;
// 						break;
// 				}
// 			}
// 			
// 			printf("d = %d\n", (unsigned int)cp[i]);
// 
// 			cp[i] = (unsigned int)cp[i];
// 			
// 			
// /*			switch ((int)cp[i]) {
// 				case -79:
// 				case -71:
// 					cp[i] = charTable[0];
// 					break;
// 				case -65:
// 					cp[i] = charTable[1];
// 					break;
// 				case -77:
// 					cp[i] = charTable[2];
// 					break;
// 				case -74:
// 				case -100:
// 					cp[i] = charTable[3];
// 					break;
// 				case -68:
// 				case -97:
// 					cp[i] = charTable[4];
// 					break;
// 				case -22:
// 					cp[i] = charTable[5];
// 					break;
// 				case -26:
// 					cp[i] = charTable[6];
// 					break;
// 				case -15:
// 					cp[i] = charTable[7];
// 					break;
// 				case -13:
// 					cp[i] = charTable[8];
// 					break;
// 				case -95:
// 				case -91:
// 					cp[i] = charTable[9];
// 					break;
// 				case -81:
// 					cp[i] = charTable[10];
// 					break;
// 				case -93:
// 					cp[i] = charTable[11];
// 					break;
// 				case -90:
// 				case -116:
// 					cp[i] = charTable[12];
// 					break;
// 				case -85:
// 				case -113:
// 					cp[i] = charTable[13];
// 					break;
// 				case -54:
// 					cp[i] = charTable[14];
// 					break;
// 				case -58:
// 					cp[i] = charTable[15];
// 					break;
// 				case -47:
// 					cp[i] = charTable[16];
// 					break;
// 				case -45:
// 					cp[i] = charTable[17];
// 					break;*/
// // 			}
// 		}
// 	}

    switch (cg->interactive->encoding)
    {
        case 1:
            break;
        case 2:
        case 3:
            convert_encoding(cp, "cp1250", "iso-8859-2");
            break;
        case 4:
		{		
            convert_encoding(cp, "utf-8", "iso-8859-2");
// 			char *cp2;
// 			cp2 = malloc((strlen(cp) + 1 * 2) * sizeof(char) * 2);
// 			(void)strcpy(cp2, cp);
//             convert_encoding(cp2, "utf-8", "iso-8859-2");
// 			printf("len1 = %d, len2 = %d\n", (int)strlen(cp), (int)strlen(cp2)); 
//             convert_encoding(cp, "utf-8", "iso-8859-2");
            break;
		}
		default:
            plain_string_pl_lv(cp);

    }
}

#endif /* USE_DIACRITICAL_MARKS */

#ifdef USE_ANSI_COLORS

char *
ansi_clear_color_string(const char *str)
{
    short spec;
    char *ns, *tmp, *ret;
        
    ns = tmp = (char *)malloc(strlen(str) + 1);
    spec = 0;
        
    while (*str != '\0' && *str) 
    {
        if (*str == '\x1b')
            spec = 1;
        else if (spec && *str == 'm')
            spec = 0;
        else if (!spec) 
        {
            *ns = *str;
            ns++;
        }
        
        str++;
    }
    
    *ns = '\0';
    
    ret = make_mstring(tmp);
    free(tmp);
    
    return ret;
}

#endif /* USE_ANSI_COLORS */

#if defined(USE_ANSI_COLORS) || defined(USE_MXP)
/**
 * Funkcja bada d�ugo�� ci�gu odrzucaj�c wszelkie znaki, kt�re
 * nie s� z tej perspektywy wa�ne.
 *
 * @param str ci�g kt�rego d�ugo�� jest obliczana
 *
 * @return d�ugo�� badanego ci�gu
 */
size_t
ansi_strlen(const char *str) 
{
    char *a;
    size_t len;
    short flag;
    
    a = (char *)str;
    flag = 0;
    len = 0;
    
    while (*a && *a != '\0')
    {
        if (flag && (*a == 'm' || *a == 'z'))
            flag = 0;
        else if (flag && *a < '0' && *a > '9')
            error("Niedozwolona kobbinacja ze znakiem '\\x1b'\n");
        else if (flag)
            flag++;
        else if (flag > 5)
            error("Niedozwolona kombinacja ze znakiem '\\x1b'\n");
        else if (*a == '\x1b')
            flag = 1;
        else
            len++;
        
        a++;
    }
    
    return len;
}
    
#endif /* defined(USE_ANSI_COLORS) || defined(USE_MXP) */
