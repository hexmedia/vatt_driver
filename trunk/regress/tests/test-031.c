#pragma strict_types

void
create()
{
    if (strchr("abcddcba", 'c') != 2)
        throw "Blad w strchr() z charem bez kolork�w.";
    
    if (strchr(set_color(30) + "abcddcba", 'c') != 2)
        throw "Blad w strchr() z charem z kolorkami.";
    
    if (strchr("abcddcba", "c") != 2)
        throw "Blad w strchr() z stringiem";
    
    if (strrchr("abcddcba", 'c') != 5)
        throw "Blad w strchr() z charem bez kolork�w.";
    
    if (strrchr(set_color(30) + "abcddcba", 'c') != 5)
        throw "Blad w strchr() z charem z kolorkami.";
    
    if (strrchr("abcddcba", "c") != 5)
        throw "Blad w strchr() z stringiem";
    
    if (strstr(set_color(30) + set_color(30) + "abcddcba", "cdd") != 2)
        throw "Blad w strstr() z kolorkami przed";
    
    if (strstr("abc" + set_color(30) + "ddcba", "cdd") != 2)
        throw "Blad w strstr() z kolorkami w �rodku";
    
    if (strstr("abcddcba", "cdd") != 2)
        throw "Blad w strstr() bez kolorkow\n";
    
    if (strstr("abcddcba", "ff") != -1)
        throw "Blad w strstr() jesli nie moze znalezc";
    
    if (strchr("abcddcba", "f") != -1)
        throw "Blad w strchr() jesli nie moze znalezc";
}
