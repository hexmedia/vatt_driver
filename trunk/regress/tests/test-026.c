#pragma strict_types

inherit "/tests/base.c.c";

void
create()
{
    string a, b, c;
    
    a = set_color(32) + "aaa" + clear_color();
    
    b = clear_color_string(a);
    c = clear_color_format(a);
        
    if (a != b || a != c || b != c)
        throw("Nie porownuje z uwzglednieniem kolorkow\n");
}
