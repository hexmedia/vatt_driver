#pragma strict_types

void
create()
{
    if ("2" ~= 2) ; else throw ("\"2\" ~= 2 == false!!\n");
    if ("2" ~= 2.0) ; else throw("\"2\" ~= 2.0 == false!!\n");
    if (2 ~= "2") ; else throw("2 ~= \"2\" == false!!\n");
    if (2 ~= 2.0) ; else throw("2 ~= 2.0 == false!!\n");
    if (2.0 ~= "2") ; else throw("2.0 ~= \"2\" == false!!\n");
    if (2.0 ~= 2) ; else throw("2.0 ~= 2 == false!!\n");
    if ("2" !~ 2) throw ("\"2\" !~ 2 == true!!\n");
    if ("2" !~ 2.0) throw("\"2\" !~ 2.0 == true!!\n");
    if (2 !~ "2") throw("2 !~ \"2\" == true!!\n");
    if (2 !~ 2.0) throw("2 !~ 2.0 == true!!\n");
    if (2.0 !~ "2") throw("2.0 !~ \"2\" == true!!\n");
    if (2.0 !~ 2) throw("2.0 !~ 2 == true!!\n");
    
    if ("a" ~= "�"); else throw ("a ~= � == true!!\n");
    if ("�" ~= "a") throw("� ~= a == false!!\n");
    if ("a" !~ "�"); else throw("a !~ � == true!!\n");
    if ("�" !~ "a"); else throw("� !~ a == true!!\n");
}
