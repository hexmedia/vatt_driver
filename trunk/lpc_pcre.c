#include "config.h"

#ifdef USE_PCRE

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "lint.h"
#include "interpret.h"
#include "mysql.h"
#include "mapping.h"
#include "simulate.h"
#include "mstring.h"

#include "lpc_pcre.h"

struct vector      *preg_match(struct lpc_preg *, const char *, char *, int *);
struct mapping     *preg_match_mapping(struct lpc_preg *, const char *, char *, int *);
struct lpc_preg    *preg_compile(struct lpc_preg *);
struct lpc_preg    *preg_alloc(void); 
void                preg_free(struct lpc_preg *);

static void
preg_error_analyze(int ret, char *err) {
    switch (ret) 
    {
        case PCRE_ERROR_NOMATCH:
            break;
        case PCRE_ERROR_NULL:
            (void)sprintf(err, "NULL");
            break;
        case PCRE_ERROR_BADOPTION:
            (void)sprintf(err, "BAD OPTION");
            break;
        case PCRE_ERROR_BADMAGIC:
            (void)sprintf(err, "BAD MAGIC");
            break;
        case PCRE_ERROR_UNKNOWN_OPCODE:
            (void)sprintf(err, "UNKNOWN OPCODE");
            break;
        case PCRE_ERROR_NOMEMORY:
            (void)sprintf(err, "NO MEMORY");
            break;
        case PCRE_ERROR_NOSUBSTRING:
            (void)sprintf(err, "NO SUBSTRING");
            break;
        case PCRE_ERROR_MATCHLIMIT:
            (void)sprintf(err, "MATCH LIMIT");
            break;
        case PCRE_ERROR_CALLOUT:
            (void)sprintf(err, "CALLOUT");
            break;
        case PCRE_ERROR_BADUTF8:
            (void)sprintf(err, "BAD UTF8");
            break;
        case PCRE_ERROR_BADUTF8_OFFSET:
            (void)sprintf(err, "BAD UTF8 OFFSET");
            break;
        case PCRE_ERROR_PARTIAL :
            (void)sprintf(err, "PARTIAL");
            break;
        case PCRE_ERROR_BADPARTIAL:
            (void)sprintf(err, "BAD PARTIAL");
            break;
        case PCRE_ERROR_INTERNAL:
            (void)sprintf(err, "INTERNAL");
            break;
        case PCRE_ERROR_BADCOUNT:
            (void)sprintf(err, "BAD COUNT");
            break;
        case PCRE_ERROR_RECURSIONLIMIT:
            (void)sprintf(err, "RECURSION LIMIT");
            break;
        case PCRE_ERROR_BADNEWLINE:
            (void)sprintf(err, "BAD NEW LINE");
            break;
        default:
            (void)sprintf(err, "Unknown PCRE Error number %d", ret);
    }
}

/**
 * Funkcja preg_match
 */
struct vector *
preg_match(struct lpc_preg *regex, const char *str, char *err, int *ovector)
{
	int 	 ret, 
			 i;
	struct vector *vec;
	
	preg_compile(regex);
    
    if (ovector != NULL)
        ovector = malloc(sizeof(int) * OVECTOR_SIZE);

    for (i = 0 ; i < OVECTOR_SIZE ; i++)
        ovector[i] = 0;
	
	ret = pcre_exec(regex->pce, regex->extra, str, strlen(str), 0, 0, ovector, OVECTOR_SIZE);
    //Ta funkcja zwraca ilo�� grup, z grup� uwzgl�dniaj�c� ca�e wyra�enie.
    
    if (ret < 0) 
    {
        if (ret == PCRE_ERROR_NOMATCH) {
            return NULL;
        }
        
        (void)preg_error_analyze(ret, err);
        
        return NULL;
    } 
    
    regex->group_count = ret;
    
	vec = allocate_array(regex->group_count);
    
	for (i = 0 ; i < regex->group_count ; i++) 
	{
		int len, j;
		
		len = (ovector[i * 2 + 1] - ovector[i * 2]) + 1;
        
 		vec->item[i].type = T_STRING;
 
 		vec->item[i].string_type = STRING_MSTRING;
 		vec->item[i].u.string = allocate_mstring(len);
 		
 		for (j = 0 ; j < len - 1 ; j++)
 		{
 			vec->item[i].u.string[j] = str[ovector[i * 2] + j];
 		}
 		
 		vec->item[i].u.string[j] = '\0';
	}
	
	return vec;	
}

struct mapping *
preg_match_mapping(struct lpc_preg *regex, const char *str, char *err, int *ovector)
{
    struct vector  *vec,
                   *ind;
    int             i, 
                    rc,
                    name_count;
    struct mapping *map;

    vec = preg_match(regex, str, err, ovector);
    
    if (vec == NULL)
        return NULL;
    
    ind = allocate_array(vec->size);
    
    for (i = 0 ; i < vec->size ; i++)
    {
        ind->item[i].type = T_NUMBER;
        ind->item[i].u.number = i;
    }
    
    rc = pcre_fullinfo(regex->pce, NULL, PCRE_INFO_NAMECOUNT, &name_count);
    
    if (rc < 0)
        error("Internal pcre_fullinfo() error %d\n", rc);
    
    if (name_count > 0)
    {//Indeksujemy tylko po nazwach na razie.
        int             rc1,
                        rc2,
                        j = 0,
                        name_entry_size;
        unsigned char  *name_table;
        
        rc1 = pcre_fullinfo(regex->pce, NULL, PCRE_INFO_NAMETABLE, &name_table);
        rc2 = pcre_fullinfo(regex->pce, NULL, PCRE_INFO_NAMEENTRYSIZE, &name_entry_size);
        
        
        rc = rc2 ? rc2 : rc1;
        
        if (rc < 0)
            error("Internal pcre_fullinfo() error %d\n", rc);
        
        for (i = 0 ; i < name_count ; i++)
        {
            int     n = 0,
                    len = 0;
                    
            //Nie wiem czemu len si� �le liczy, ale przelicze go od nowa.
            while (*(name_table + 2 + i * name_entry_size + len) != '\0') {
                len++;
            }
            
            n = *(name_table + 1 + i * name_entry_size);
            
            ind->item[n].type = T_STRING;
            ind->item[n].string_type = STRING_MSTRING;
            ind->item[n].u.string = allocate_mstring(len + 1);
            
            j = 0;
            while (*(name_table + 2 + i * name_entry_size + j) != '\0') {
                ind->item[n].u.string[j] = *(name_table + 2 + i * name_entry_size + j);
                j++;
            }
       
            ind->item[n].u.string[j] = '\0';
        }
        
        for (i = 0, j = 0 ; i < ind->size ; i++) {
           if (ind->item[i].type != T_STRING) {
               ind->item[i].type = T_NUMBER;
               ind->item[i].u.number = j;
               j++;
           }
        }
    }
    
    map = make_mapping(ind, vec);

    return map;
}

char * 
preg_replace(struct lpc_preg *regex, const char *str, char *err, struct mapping *rep, int *ovector) 
{
    int     ret,
            size,
            rc, 
            i,
            name_count;
    char   *ex;

    preg_compile(regex);
    
    if (ovector == NULL) 
        ovector = malloc(sizeof(int) * OVECTOR_SIZE);
    
    for (i = 0 ; i < OVECTOR_SIZE ; i++)
        ovector[i] = -1;
    
    ret = pcre_exec(regex->pce, regex->extra, str, strlen(str), 0, 0, ovector, OVECTOR_SIZE);
    //Ta funkcja zwraca ilo�� grup, z grup� uwzgl�dniaj�c� ca�e wyra�enie.
    
    if (ret < 0) 
    {
        if (ret == PCRE_ERROR_NOMATCH) {
            return NULL;
        }
        
        (void)preg_error_analyze(ret, err);
        
        return NULL;
    } 
    
    regex->group_count = ret;
    
    size = strlen(str);
    
    for (i = 3 ; ovector[i] != -1 ; i++)
    {
        if (i % 2 == 1)
            size -= (ovector[i] - ovector[i - 1]);
    }
    
    for (i = 0 ; i < rep->size ; i++)
    {
        if (rep->pairs[i]->val.type == T_STRING) 
            size += strlen(rep->pairs[i]->val.u.string);
    }
    
    size++;
    
    rc = pcre_fullinfo(regex->pce, NULL, PCRE_INFO_NAMECOUNT, &name_count);
        
    if (rc < 0)
        error("Internal pcre_fullinfo() error %d\n", rc);
    
    ex = allocate_mstring(size);
    
    {
        int             rc1,
                        rc2,
                        j,
                        k,
                        name_entry_size;
        unsigned char  *name_table;
        
        rc1 = pcre_fullinfo(regex->pce, NULL, PCRE_INFO_NAMETABLE, &name_table);
        rc2 = pcre_fullinfo(regex->pce, NULL, PCRE_INFO_NAMEENTRYSIZE, &name_entry_size);
        
        rc = rc2 ? rc2 : rc1;
        
        if (rc < 0)
            error("Internal pcre_fullinfo() error %d\n", rc);
        
        for (i = 0 ; i < name_count ; i++)
        {
            int     n = 0;
            char   *name;
            
            n = *(name_table + 1 + i * name_entry_size);
            name = (char *)(name_table + 2 + i * name_entry_size);
        }
        
        
        for (i = 0, j = 0, k = 2 ; i < size - 1 ; i++) 
        {
            if (j == ovector[k]) 
            { //Tu co� wstawiamy
                j = ovector[k + 1];
            }
        }
    }
        
        
    ex[size - 1] = '\0';
    
    return ex;
}
    
    
/**
 * Funkcja odpowiedzialna za skompilowanie tekstu.
 */
struct lpc_preg *
preg_compile(struct lpc_preg *ret) {
	const char     *compile_errptr;
	int             compile_erroffset,
                    pce_options = 0,
//                     do_study,
                    i;
                    
    //Sprawdzamy czy wersja z opcjami czy bez
    if (ret->regex[0] == '/') { 
        ret->regex++;
            
        for (i = strlen(ret->regex) - 1 ; i >= 0 && ret->regex[i] != '/' ; i--) {
            switch (ret->regex[i]) {
                /* Perl compatible options */
                case 'i':   pce_options |= PCRE_CASELESS;       break;
                case 'm':   pce_options |= PCRE_MULTILINE;      break;
                case 's':   pce_options |= PCRE_DOTALL;         break;
                case 'x':   pce_options |= PCRE_EXTENDED;       break;
                
                /* PCRE specific options */
                case 'A':   pce_options |= PCRE_ANCHORED;       break;
                case 'D':   pce_options |= PCRE_DOLLAR_ENDONLY; break;
//                 case 'S':   do_study  = 1;                      break;
                case 'U':   pce_options |= PCRE_UNGREEDY;       break;
                case 'X':   pce_options |= PCRE_EXTRA;          break;
                case 'u':   pce_options |= PCRE_UTF8;           break;

                case ' ':
                case '\n':
                    break;
                
                default:
                    error("Nieznana opcja '%c'\n", ret->regex[i]);
            }
        }
    
        ret->regex[i] = '\0';
    }
    
//     if (do_study) {
//         extra = pcre_study(ret->pce, soptions, &error);
//         if (extra) {
//             extra->flags |= PCRE_EXTRA_MATCH_LIMIT | PCRE_EXTRA_MATCH_LIMIT_RECURSION;
//         }
//         if (error != NULL) {
//             php_error_docref(NULL TSRMLS_CC, E_WARNING, "Error while studying pattern");
//         }
//     } else {
//         extra = NULL;
//     }
    
	if (ret->pce == NULL) {
        ret->pce = pcre_compile(ret->regex, pce_options, &compile_errptr, &compile_erroffset, NULL);
        
		if (!ret->pce) {
			error("PCRE compilation failed at offset %d: \"\e[31m%s\e[0m\"\n", compile_erroffset, compile_errptr);
		}
	}
	
	return ret;
}
	
struct lpc_preg *
preg_alloc(void) 
{
	struct lpc_preg *r;
	
	r = malloc(sizeof(struct lpc_preg));
	
	r->pce 			= NULL;
	r->regex 		= NULL;
	r->extra 		= NULL;
	r->group_count 	= 0;
	
	return r;
}
	
	
void
preg_free(struct lpc_preg *r) {
    if (r != NULL) 
    {
        if (r->pce != NULL)
            pcre_free(r->pce);
        
        r->pce = NULL;
        
        free(r);
    }
}
#endif /* USE_PCRE */


