#include <stdlib.h>
#include <arpa/telnet.h>

#include "mccp.h"
#include "ndesc.h"
#include "telnet.h"

int
compress_start(struct interactive* player)
{
  z_stream* stream
    ;

  if (((telnet_t*) player->tp)->s)
    return 1;

  stream = (z_stream *) calloc(1, sizeof(*stream));

  stream->next_in = NULL;
  stream->avail_in = 0;

  stream->next_out = player->s_buf;
  stream->avail_out = S_BUF_SIZE;

  stream->zalloc = Z_NULL;
  stream->zfree  = Z_NULL;
  stream->opaque = Z_NULL;

  if (deflateInit(stream, MCCP_LEVEL) != Z_OK) {
    free(stream);
    return 0;
  }
  
  nq_putc(((telnet_t*) player->tp)->t_outq, IAC, NULL);
  nq_putc(((telnet_t*) player->tp)->t_outq, SB, NULL);
  nq_putc(((telnet_t*) player->tp)->t_outq, 86, NULL);
  nq_putc(((telnet_t*) player->tp)->t_outq, IAC, NULL);
  nq_putc(((telnet_t*) player->tp)->t_outq, SE, NULL);

  ((telnet_t*) player->tp)->s = stream;
  return 1;
}

int
compress_end(struct interactive* player)
{
  z_stream* stream;
  unsigned char dummy[1];
  
  if (!(((telnet_t*) player->tp)->s))
    return 1;

  ((telnet_t*) player->tp)->s->avail_in = 0;
  ((telnet_t*) player->tp)->s->next_in = dummy;
  ((telnet_t*) player->tp)->s->next_out = ((telnet_t*) player->tp)->s->next_out -
    (S_BUF_SIZE - ((telnet_t*) player->tp)->s->avail_out);
  ((telnet_t*) player->tp)->s->avail_out = S_BUF_SIZE;

  if (deflate(((telnet_t*) player->tp)->s, Z_FINISH) != Z_STREAM_END)
    return 0;
  
  stream = ((telnet_t*) player->tp)->s;
  ((telnet_t*) player->tp)->s = NULL;

  nq_realputs(((telnet_t*) player->tp)->t_outq, player->s_buf, S_BUF_SIZE - stream->avail_out);

  deflateEnd(stream);
  free(stream);

  return 1;
}

int
mccp_compress(nqueue_t *nq, z_stream* s, unsigned char* data, int length)
{
	s->next_in = data;
  s->avail_in = length;
  s->next_out = s->next_out - (S_BUF_SIZE - s->avail_out);
  s->avail_out = S_BUF_SIZE;

  while ((s->avail_in > 0) && (s->avail_out > 0))
  {
    if (deflate(s, Z_SYNC_FLUSH) != Z_OK)
    {
      return 0;
    }
  }

  if (S_BUF_SIZE - s->avail_out > 0) {
    nq_realputs(nq, s->next_out - (S_BUF_SIZE - s->avail_out), S_BUF_SIZE - s->avail_out);
  }
  return 1;
}
