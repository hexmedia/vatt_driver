#ifdef USE_DICTIONARY

#ifndef __DICTIONARY_H__
#define __DICTIONARY_H__
struct dictionary {
    struct dictionary *prev;
    struct dictionary *next;
    struct dict_word *word;
};

struct dict_word {
    char *lp_mia;
    char *lp_dop;
    char *lp_cel;
    char *lp_bie;
    char *lp_nar;
    char *lp_mie;
    
    char *mn_mia;
    char *mn_dop;
    char *mn_cel;
    char *mn_bie;
    char *mn_nar;
    char *mn_mie;
    
    short rodz;
    
    short mn;
};

struct dict_wordr {
    struct dict_word *word;
    short mn;
};

struct dict_wordr  *dictionary_search(char *);
short               dictionary_check(char *);
void                dictionary_reindex(void);

#define DICTIONARY_MYSQL_USER       "root"
#define DICTIONARY_MYSQL_PASSWD     "wizard2d"
#define DICTIONARY_MYSQL_DBNAME     "slownik"
#define DICTIONARY_MYSQL_DBHOST     NULL
#define DICTIONARY_MYSQL_DBPORT     0

#endif /*__DICTIONARY_H__*/

#endif /*USE_DICTIONARY*/
