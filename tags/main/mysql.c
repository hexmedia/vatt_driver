
#include "config.h"

#ifdef USE_MYSQL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>
#include <mysql/errmsg.h>
#include <ctype.h>

#include "lint.h"
#include "interpret.h"
#include "mysql.h"
#include "mapping.h"
#include "simulate.h"
#include "mstring.h"

struct mysql_handle *mysql_handler = NULL;
struct mysql_handle *last_mysql_handler = NULL;

int next_indent = 1;

// short
// lpc_mysql_init()
// {
//     const char *client_version = mysql_get_client_version();
//     const char *server_version = MYSQL_SERVER_VERSION
// #ifdef MYSQL_SERVER_SUFFIX
//         MYSQL_SERVER_SUFFIX
// #endif /* MYSQL_SERVER_SUFFIX */
//         ;
//     
//     long cl_version, sv_version;
//     
//     cl_version = strtol(client_version, NULL, 10);
//     sv_version = strtol(server_version, NULL, 10);
//     
//     if (cl_version != sv_version)
//     {
//         //TODO: Jak b�dziemy mieli mo�liwo�� wyrzucania warning�w,
//         //      to tu co� takiego trzeba b�dzie da�.
//         
//        if (cl_version > sv_version)
//            return 0;
//     }
//     
//     return 1;
// }

/**
 * Zwalnia handler mysql'a
 *
 * @return 0/1/2
 *  0 - nie uda�o si� zwolni�
 *  1 - zwolniony
 *  2 - ju� pusty
 */
short
free_mysql_handle(struct mysql_handle *h) 
{
    if (!h)
        return 2;
    
    if (h->prev != NULL && h->next != NULL)
    {
        h->next->prev = h->prev;
        h->prev->next = h->next;
    }
    else if (h->prev != NULL)
    {
        if (last_mysql_handler->ident == h->ident)
            last_mysql_handler = h->prev;
        
        h->prev->next = NULL;
    }
    else if (h->next != NULL)
    {
        if (mysql_handler->ident == h->ident)
            mysql_handler = h->next;
        h->next->prev = NULL;
    }
    
    if (h->result != NULL)
    {
        mysql_free_result(h->result);
    }
    
    free(h);
    
    h = NULL;
    
    return 1;
}

static struct mysql_handle *
allocate_next_mysql_handle(void)
{
    struct mysql_handle *h;
    
    h = (struct mysql_handle *)malloc(sizeof(struct mysql_handle));
    
    if (!h)
    {
        error("Out of memory!\n");
        return NULL;
    }
    
    h->handler = mysql_init(NULL);
    
    if (h->handler == NULL)
    {
        mysql_close(h->handler);
        error("Out of memory!\n");
    }

    h->next = NULL;
    h->row = NULL;
    h->result = NULL;
    h->ident = next_indent++;
    
    if (last_mysql_handler)
    {
        h->prev = last_mysql_handler;
        last_mysql_handler->next = h;
    }
    else if (mysql_handler)
        printf("KURWA.\n");
    else
        mysql_handler = h;
    
    last_mysql_handler = h;
   
    return h;
}

struct mysql_handle *
get_mysql_handle_by_ident(int ident)
{
    struct mysql_handle *h;
    
    h = mysql_handler;
    
    if (h)
    {
        do
        {
            if (h->ident == ident)
                return h;
            
        } while((h = h->next) != NULL);
    }
    
    return NULL;
}


static short
mysql_type_to_lpc_type(int mt)
{
    switch (mt)
    {
        case MYSQL_TYPE_TINY:
        case MYSQL_TYPE_SHORT:
        case MYSQL_TYPE_LONG:
        case MYSQL_TYPE_INT24:
        case MYSQL_TYPE_LONGLONG:
        case MYSQL_TYPE_DECIMAL: 
        case MYSQL_TYPE_NEWDECIMAL:
        case MYSQL_TYPE_BIT:
        case MYSQL_TYPE_YEAR:
            return T_NUMBER;
            
        case MYSQL_TYPE_FLOAT:
        case MYSQL_TYPE_DOUBLE:
            return T_FLOAT;
            
        default:
            return T_STRING;
    }
}
            
struct mysql_handle *
assign_mysql_handle(struct mysql_handle *h, int *ident)
{
    if (h == NULL)
    {
        if (*ident == 0)
        {
            *ident = last_mysql_handler->ident;
            h = last_mysql_handler;
        }
        else
            h = get_mysql_handle_by_ident(*ident);
    }
    
    return h;
}

static void
check_handler(struct mysql_handle *h)
{
    if (h == NULL)
        error("Brak handlera MySQL.\n");

    if (h->handler == NULL)
        error("Po��czenie nie zosta�o nawi�zane.\n");
}

/**
 * Funkcja ��cz�ca z baz� danych.
 *
 * @return 1/0 (sukces/pora�ka)
 */
int
lpc_mysql_connect(char *user, char *passwd, char *dbname, char *host, unsigned int port)
{
    struct mysql_handle *h;

    h = allocate_next_mysql_handle();
    
    check_handler(h);
    
    if (mysql_real_connect(h->handler, host, user, passwd, dbname, port, NULL, 0) == NULL)
    {
        int err = mysql_errno(h->handler);
        mysql_close(h->handler);
        free_mysql_handle(h);
        
        error("Error %d.\n", err);
        
//         if (conn == CR_CONN_HOST_ERROR)
//             error("Pr�ba po��czenia z serwerem MySQL nie powiod�a si�.\n");
//         else if (conn == CR_CONNECTION_ERROR)
//             error("Pr�ba po�aczenia z lokalnym serwerem MySQL nie powido�a si�.\n");
//         else if (conn == CR_IPSOCK_ERROR)
//             error("Nie uda�o si� utworzy� ip socketa.\n");
//         else if (conn == CR_OUT_OF_MEMORY)
//             error("Out of memory!\n");
//         else if (conn == CR_SOCKET_CREATE_ERROR)
//             error("Nie uda�o si� utworzy� unixowego socketa.\n");
//         else if (conn == CR_UNKNOWN_HOST)
//             error("Nie uda�o si� znale�� hosta.\n");
//         else if (conn == CR_VERSION_ERROR)
//             error("B��d wersji!\n");
//         else if (conn == CR_LOST_SERVER)
//             error("Po��cznie z serwerem zosta�o utracone.\n");
//         else 
//             error("Nie uda�o si� po��czy� z serwerem. Pow�d nieznany.\n");

        return 0;
    }
    
    lpc_mysql_set_charset("latin2", 0, h);
    
    return h->ident;
}

int
lpc_mysql_select_db(char *db, int ident, struct mysql_handle *h)
{    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return (int)mysql_select_db(h->handler, db);
}

int
lpc_mysql_unbuffered_query(char *query, int ident, struct mysql_handle *h)
{    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return (int)mysql_real_query(h->handler, query, strlen(query));
}


int 
lpc_mysql_query(char *query, int ident, struct mysql_handle *h)
{
    int err;
    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    err = (int)mysql_real_query(h->handler, query, strlen(query));
    
    if (err == 0)
        h->result = mysql_store_result(h->handler);
    
    return err;
}

void *
lpc_mysql_fetch_row(int type, int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    if (h->result == NULL) 
        return NULL;
    
    h->row = mysql_fetch_row(h->result);
    
    if (h->row == NULL || !h->row)
    {
        mysql_free_result(h->result); //Automatycznie przy ostatnim czy�cimy.
        h->result = NULL;
        
        return NULL;
    }
    
    {
        int i, num_fields;
        struct vector *vec;
        struct vector *ind;
        
        num_fields = mysql_num_fields(h->result);
        
        vec = allocate_array(num_fields);
              
        if (!vec)
        {
            error("Out of memory.!");
            return NULL;
        }
        
        {
            MYSQL_FIELD *fields;
            
            fields = mysql_fetch_fields(h->result);
            
            ind = NULL; //coby nie warningowa�o.
            
            if (type == LPC_MYSQL_FT_MAPPING)
            {
                ind = allocate_array(num_fields);
                
                for (i = 0 ; i < num_fields ; i++)
                {
                    struct svalue *item;
                    
                    item = &ind->item[i];
                    
                    item->type = T_STRING;
                    item->string_type = STRING_MSTRING;
                    item->u.string = make_mstring(fields[i].name);
                }
            }
            
            for (i = 0 ; i < num_fields ; i++)
            {
                struct svalue *item;
                
                item = &vec->item[i];
                
                item->type = mysql_type_to_lpc_type(fields[i].type);
                
                switch (item->type) {
                    case T_NUMBER:
                        item->u.number = (long long)strtoll(h->row[i], &((char *)h->row[i]), 10);
                        break;
                    case T_FLOAT:
                        item->u.real = atof(h->row[i]);
                        break;
                    case T_STRING:
                        item->u.string = make_mstring(h->row[i]);
                        item->string_type = STRING_MSTRING;
                        break;
                }
            }
            
            if (type == LPC_MYSQL_FT_MAPPING)
            {
                struct mapping *map;
                
                map = make_mapping(ind, vec);
                
                free_vector(ind);
                free_vector(vec);
                
                return map;
            }
            else
            {
                return vec;
            }
        }
    }
}

int
lpc_mysql_begin(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return mysql_real_query(h->handler, "BEGIN", 5);
}

int
lpc_mysql_commit(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return mysql_real_query(h->handler, "COMMIT", 6);
}

int
lpc_mysql_rollback(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return mysql_real_query(h->handler, "ROLLBACK", 8);
}

int
lpc_mysql_autocommit(short mode, int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return mysql_autocommit(h->handler, mode);
}

int
lpc_mysql_errno(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return (int)mysql_errno(h->handler);
}

const char *
lpc_mysql_error(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    return (const char *)mysql_error(h->handler);
}

long long
lpc_mysql_insert_id(int ident, struct mysql_handle *h)
{
    long long err;
    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    err = (long long)mysql_insert_id(h->handler);
    
    return err;
}

long long
lpc_mysql_num_rows(int ident, struct mysql_handle *h)
{
    long long err;
    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    err = (long long)mysql_num_rows(h->result);
    
    return err;
}

long long
lpc_mysql_affected_rows(int ident, struct mysql_handle *h)
{
    long long err;
    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    err = (long long)mysql_affected_rows(h->handler);
    
    return err;
}

void
lpc_mysql_free_result(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    mysql_free_result(h->result);
}

char *
lpc_mysql_real_escape_string(char *str, int ident, struct mysql_handle *h)
{
    int len, err;
    char *ret;
    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    len = strlen(str);
    
    ret = (char *)malloc(sizeof(char) * len * 2 + 1);
    
    err = (int)mysql_real_escape_string(h->handler, ret, str, len);
    
    if (err) {
        return NULL;
    }
    
    free_mstring(str);
    
    return ret;
}

int
lpc_mysql_set_charset(char *cs, int ident, struct mysql_handle *h)
{
    int err;
    
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    err = mysql_set_character_set(h->handler, cs);
    
    return err; 
}

void
lpc_mysql_close(int ident, struct mysql_handle *h)
{
    h = assign_mysql_handle(h, &ident);
    
    check_handler(h);
    
    if (h->result != NULL)
    {
        mysql_free_result(h->result);
        h->result = NULL;
    }
    
    mysql_close(h->handler);
}
    
#endif /*USE_MYSQL*/
