
#ifdef USE_MYSQL

#include <mysql/mysql.h>

#define LPC_MYSQL_FT_STANDARD   0
#define LPC_MYSQL_FT_MAPPING    1

struct mysql_handle {
    struct mysql_handle *prev;
    struct mysql_handle *next;
    
    MYSQL *handler;
    MYSQL_RES *result;
    MYSQL_ROW row;
    
    int ident;
};

struct mysql_row {
    struct mysql_field *val;
};

struct mysql_field {
    int type;
    struct svalue *val;
};

/*
 * Lista funkcji:
 */
extern int              lpc_mysql_connect(char *, char *, char *, char *, unsigned int);
extern int              lpc_mysql_select_db(char *, int, struct mysql_handle *);
extern int              lpc_mysql_unbuffered_query(char *, int, struct mysql_handle *);
extern int              lpc_mysql_query(char *, int, struct mysql_handle *);
extern void            *lpc_mysql_fetch_row(int, int, struct mysql_handle *);
extern int              lpc_mysql_begin(int, struct mysql_handle *);
extern int              lpc_mysql_commit(int, struct mysql_handle *);
extern int              lpc_mysql_rollback(int, struct mysql_handle *);
extern int              lpc_mysql_autocommit(short, int, struct mysql_handle *);
extern int              lpc_mysql_errno(int, struct mysql_handle *);
extern const char      *lpc_mysql_error(int, struct mysql_handle *);
extern long long        lpc_mysql_insert_id(int, struct mysql_handle *);
extern long long        lpc_mysql_num_rows(int, struct mysql_handle *);
extern long long        lpc_mysql_affected_rows(int, struct mysql_handle *);
extern void             lpc_mysql_free_result(int, struct mysql_handle *);
extern char            *lpc_mysql_real_escape_string(char *, int, struct mysql_handle *);
extern int              lpc_mysql_set_charset(char *, int, struct mysql_handle *);
extern void             lpc_mysql_close(int, struct mysql_handle *);

extern struct mysql_handle * get_mysql_handle_by_ident(int);

#endif /*USE_MYSQL*/
