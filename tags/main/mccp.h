#ifndef _JS_MCCP_SUPPORT_H
#define _JS_MCCP_SUPPORT_H

#define MCCP_LEVEL     9

#include "comm.h"
#include "nqueue.h"

int compress_start(struct interactive* player);
int compress_end(struct interactive* player);
int mccp_compress(nqueue_t *nq, z_stream* s, unsigned char* data, int length);

#endif
