
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>

// #include "interpret.h"
#include "mstring.h"

#ifdef USE_ANSI_COLORS

char *
ansi_clear_color_string(char *str)
{
    short spec;
    char *ns, *tmp, *ret;
        
    ns = tmp = (char *)malloc(strlen(str) + 1);
    spec = 0;
        
    while (*str != '\0' && *str) 
    {
        if (*str == '\x1b')
            spec = 1;
        else if (spec && *str == 'm')
            spec = 0;
        else if (!spec) 
        {
            *ns = *str;
            ns++;
        }
        
        str++;
    }
    
    *ns = '\0';
    
    ret = make_mstring(tmp);
    free(tmp);
    
    return ret;
}

#endif /* USE_ANSI_COLORS */
