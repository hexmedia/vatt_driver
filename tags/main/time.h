/**
* Plik ten jest cz�ci� silnika muda Vatt'ghern(vattghern.pl).
*
* (c) 2008 Krystian "Krun" Kuczek  (krun@vattghern.pl)
* All rights reserved (r).
*
* Niekt�re funkcje zosta�y przeniesione z innych plik�w
* aby wprowadzi� troch� wi�kszy porz�dek w mudlibie.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* and exceptions are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. The name of the authors may not be used to endorse or promote products
*    derived from this software without specific prior written permission.
* 4. The code can not be used by Gary Random, Random Communications, Inc.,
*    the employees of Random Communications, Inc. or its subsidiaries,
*    including Defiance MUD, without prior written permission from the
*    authors.
*
* THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
* AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
* THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
* EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
* OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


struct vector      *lpc_localtime(time_t);
time_t              lpc_mktime(int, int, int, int, int, int);
char               *ctime_string(time_t, int);
char               *time_string(time_t, char *);
