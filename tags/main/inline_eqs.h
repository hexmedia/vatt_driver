#include "stringpl.h"

#if defined(PROFILE)
int equal_svalue(const struct svalue *, const struct svalue *);
int equal_svalue_alike(const struct svalue *, const struct svalue *);
int equal_svalue_nsym_pl(const struct svalue *, const strucnt svalue *);
#else /* PROFILE */
static __inline__ int
equal_svalue(const struct svalue *sval1, const struct svalue *sval2)
{
    if (sval1->type == T_NUMBER && sval1->u.number == 0 && sval2->type == T_OBJECT && sval2->u.ob->flags & O_DESTRUCTED)
        return 1;
    else if (sval2->type == T_NUMBER && sval2->u.number == 0 && sval1->type == T_OBJECT && sval1->u.ob->flags & O_DESTRUCTED)
        return 1;
    else if (sval1->type == T_NUMBER && sval1->u.number == 0 && sval2->type == T_FUNCTION && !legal_closure(sval2->u.func))
        return 1;
    else if (sval2->type == T_NUMBER && sval2->u.number == 0 && sval1->type == T_FUNCTION && !legal_closure(sval1->u.func))
        return 1;
    else if (sval1->type != sval2->type)
        return 0;
    else switch (sval1->type) {
        case T_NUMBER:
            return sval1->u.number == sval2->u.number;
        case T_POINTER:
            return sval1->u.vec == sval2->u.vec;
        case T_MAPPING:
            return sval1->u.map == sval2->u.map;
        case T_STRING:
            return sval1->u.string == sval2->u.string ||
               strcmp(sval1->u.string, sval2->u.string) == 0;
        case T_OBJECT:
            return ((sval1->u.ob->flags & O_DESTRUCTED) && (sval2->u.ob->flags & O_DESTRUCTED)) ||
               sval1->u.ob == sval2->u.ob;
        case T_FLOAT:
            return sval1->u.real == sval2->u.real;
        case T_FUNCTION:
            return sval1->u.func == sval2->u.func;
        }
    return 0;
}

static __inline__ int
equal_svalue_alike(const struct svalue *sval1, const struct svalue *sval2)
{
    if(equal_svalue(sval1, sval2))
        return 1;

    switch(sval1->type)
    {
        case T_STRING:
            switch(sval2->type)
            {
                case T_NUMBER:
                {
                    long long l;
                    l = atoll(sval1->u.string);
                    return (l == sval2->u.number);
                }
                case T_FLOAT:
                {
                    float f;
                    f = (float)atof(sval1->u.string);
                    return (f == (float)sval2->u.real);
                }
                 case T_STRING:
                     return (int)strcmp_nsym_pl((char *)sval1->u.string, (char *)sval2->u.string) == 0;
            }
            return 0;

        case T_FLOAT:
            switch(sval2->type)
            {
                case T_NUMBER:
                    return (sval1->u.real == (float)sval2->u.number);
                case T_STRING:
                {
                    float f;
                    f = (float)atof(sval2->u.string);
                    return (sval1->u.real == f);
                }
            }
            return 0;

        case T_NUMBER:
            switch(sval2->type)
            {
                case T_FLOAT:
                    return ((float)sval1->u.number == sval2->u.real);
                case T_STRING:
                {
                    long long l;
                    l = (long long)atoll(sval2->u.string);
                    return ((int)sval1->u.number == l);
                }
            }
            return 0;

        default:
            return 0;
    }
}

static __inline__ int
equal_svalue_nsym_pl(const struct svalue *sval1, const struct svalue *sval2)
{
    if (sval1->type == T_NUMBER && sval1->u.number == 0 && sval2->type == T_OBJECT && sval2->u.ob->flags & O_DESTRUCTED)
        return 1;
    else if (sval2->type == T_NUMBER && sval2->u.number == 0 && sval1->type == T_OBJECT && sval1->u.ob->flags & O_DESTRUCTED)
        return 1;
    else if (sval2->type == T_NUMBER && sval2->u.number == 0 && sval1->type == T_FUNCTION && !legal_closure(sval1->u.func))
        return 1;
    else if (sval2->type == T_NUMBER && sval2->u.number == 0 && sval1->type == T_FUNCTION && !legal_closure(sval1->u.func))
        return 1;
    else if (sval1->type != sval2->type)
        return 0;
    else switch (sval1->type) {
        case T_NUMBER:
            return sval1->u.number == sval2->u.number;
        case T_POINTER:
            return sval1->u.vec == sval2->u.vec;
        case T_MAPPING:
            return sval1->u.map == sval2->u.map;
        case T_STRING:
            return sval1->u.string == sval2->u.string ||
               strcmp_nsym_pl(sval1->u.string, sval2->u.string) == 0;
        case T_OBJECT:
            return ((sval1->u.ob->flags & O_DESTRUCTED) && (sval2->u.ob->flags & O_DESTRUCTED)) ||
               sval1->u.ob == sval2->u.ob;
        case T_FLOAT:
            return sval1->u.real == sval2->u.real;
        case T_FUNCTION:
            return sval1->u.func == sval2->u.func;
    }
    return 0;
}
#endif /* PROFILE */
