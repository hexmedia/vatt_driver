#pragma strict_types

void
create()
{
    if ("2" ~= 2) ; else throw ("\"2\" ~= 2 == false!!");
    if ("2" ~= 2.0) ; else throw("\"2\" ~= 2.0 == false!!");
    if (2 ~= "2") ; else throw("2 ~= \"2\" == false!!");
    if (2 ~= 2.0) ; else throw("2 ~= 2.0 == false!!");
    if (2.0 ~= "2") ; else throw("2.0 ~= \"2\" == false!!");
    if (2.0 ~= 2) ; else throw("2.0 ~= 2 == false!!");
    if ("2" !~ 2) throw ("\"2\" !~ 2 == true!!");
    if ("2" !~ 2.0) throw("\"2\" !~ 2.0 == true!!");
    if (2 !~ "2") throw("2 !~ \"2\" == true!!");
    if (2 !~ 2.0) throw("2 !~ 2.0 == true!!");
    if (2.0 !~ "2") throw("2.0 !~ \"2\" == true!!");
    if (2.0 !~ 2) throw("2.0 !~ 2 == true!!");
    
    if ("a" ~= "�") throw ("a ~= � == true!!");
    if ("�" ~= "a"); else throw("� ~= a == false!!");
    if ("a" !~ "�"); else throw("a != � == true!!");
    if ("�" !~ "a") throw("� != a == true!!");
}
