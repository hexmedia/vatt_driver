#pragma strict_types

void
create()
{
    if ("2" ~= 2) ; else throw ("\"2\" ~= 2 == false!!");
    if ("2" ~= 2.0) ; else throw("\"2\" ~= 2.0 == false!!");
    if (2 ~= "2") ; else throw("2 ~= \"2\" == false!!");
    if (2 ~= 2.0) ; else throw("2 ~= 2.0 == false!!");
    if (2.0 ~= "2") ; else throw("2.0 ~= \"2\" == false!!");
    if (2.0 ~= 2) ; else throw("2.0 ~= 2 == false!!");
    if ("2" !~ 2) throw ("\"2\" !~ 2 == true!!");
    if ("2" !~ 2.0) throw("\"2\" !~ 2.0 == true!!");
    if (2 !~ "2") throw("2 !~ \"2\" == true!!");
    if (2 !~ 2.0) throw("2 !~ 2.0 == true!!");
    if (2.0 !~ "2") throw("2.0 !~ \"2\" == true!!");
    if (2.0 !~ 2) throw("2.0 !~ 2 == true!!");
    
    if ("a" ~= "ą") throw ("a ~= ą == true!!");
    if ("ą" ~= "a"); else throw("ą ~= a == false!!");
    if ("a" !~ "ą"); else throw("a != ą == true!!");
    if ("ą" !~ "a") throw("ą != a == true!!");
}
