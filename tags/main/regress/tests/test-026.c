#pragma strict_types

inherit "/tests/base.c.c";

void
create()
{
    string a, b, c;
    
    a = set_color(32) + "aaa" + clear_color();
    
    dump_array(a);
    b = clear_color_string(a);
    dump_array(b);
    c = clear_color_format(a);
    dump_array(c);
    
    
    if (a != b || a != c || b != c)
        throw("Nie porownuje z uwzglednieniem kolorkow");
}
