/**
 * Plik ten jest cz�ci� silnika muda Vatt'ghern(vattghern.pl).
 *
 * (c) 2008 Krystian "Krun" Kuczek  (krun@vattghern.pl)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * and exceptions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the authors may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 4. The code can not be used by Gary Random, Random Communications, Inc.,
 *    the employees of Random Communications, Inc. or its subsidiaries,
 *    including Defiance MUD, without prior written permission from the
 *    authors.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * \file slownik.c
 *
 * W pliku tym znajduj� si� funkcje odpowiedzialne za dzia�anie opartego
 * o mysql s�ownika driverowego.
 *
 * @author  Krun
 * @date    17.04.2008
 * @version 1.0
 *
 * @date    14.05.2008
 * @version 1.5
 *
 * @date    16.09.2008
 * @version 1.6
 */

#include <string.h>
#include "config.h"

#include <stdio.h>
#include <mysql/mysql.h>

#include "object.h"
#include "lint.h"
#include "mstring.h"
#include "interpret.h"
#include "exec.h"
#include "mudstat.h"
#include "main.h"
#include "simulate.h"
#include "backend.h"
#include "slownik.h"

#define SPRAWDZ_SLOWNIK(conn)                                                       \
    if(conn == NULL || !conn)                                                       \
{                                                                               \
        error("Nie uda�o si� nawi�za� po��czenia z baz� danych s�ownika.\n");       \
}

#define SLOWO_BLEDNIE_W_SLOWNIKU(sl, mia, res)                                      \
    if(sl == NULL)                                                                  \
{                                                                               \
        mysql_free_result(res);                                                     \
        error("S�owo `%s` zosta�o b��dnie zapisane w s�owniku.\n", mia);            \
}

#define SLOWA_NIE_MA_W_SLOWNIKU(sp, mia, res)                                       \
    if(!sp)                                                                         \
{                                                                               \
        mysql_free_result(res);                                                     \
        error("S�owa `%s` nie ma w s�owniku.\n", mia);                              \
}

#define SPRAWDZ_TYP(sp, fun)                                                        \
    if(sp->type != T_STRING)                                                        \
{                                                                               \
        error("Dopusczalny typ mianownika do funkcji %s() to string.\n", fun);      \
}

#define SPRAWDZ_DLUGOSC(mia, fun)                                                   \
    if(strlen(mia) > 30)                                                            \
{                                                                               \
        error("Maksymalna przewidziana d�ugo�� s�owa do %s() to 20 znak�w. U�yte s�owo `%s` ma ich %d.\n", fun, mia, (int)strlen(mia));  \
}

/**
 * Funkcja pomocnicza pozwalaj�ca na po��czenie z baz� danych.
 *
 */
MYSQL *
polacz_slownik(void)
{
    MYSQL *conn;

    conn = NULL;

    conn = mysql_init(0);

    if(!conn)
        return NULL;

    conn = mysql_real_connect(conn, NULL,
                              SLOWNIK_MYSQL_USER, SLOWNIK_MYSQL_PASS, SLOWNIK_MYSQL_DB,
                              SLOWNIK_MYSQL_PORT, SLOWNIK_MYSQL_SOCKET, 0);

    if(!conn)
    {
        mysql_close(conn);
        return NULL;
    }

    mysql_real_query(conn, "SET NAMES latin2", 16);

    return conn;
}

/**
 * Funkcja pomocnicza roz��czaj�ca z baz� danych.
 */
void
        rozlacz_slownik(MYSQL *conn)
{
    mysql_close(conn);
}

/**
 * Funkcja ma za zadanie pobra� odmiane s�owa ze s�ownika.
 *
 * @param mianownik - mianownik w liczbie pojedynczej lub mnogiej.
 *
 * @return pe�na odmiana, tablica 2 elementowa je�li s�owo by�o podane w liczbie mnogiej
 *         lub tylko w takiej liczbie wyst�puje, 3 elementowa w przeciwnym
 *         wypadku.
 */
struct vector *
        slownik_pobierz(struct svalue *mianownik)
{
    int             i,
    j;
    char           *mia,
    query[352];
    struct vector  *vec;
    MYSQL          *conn;
    MYSQL_ROW       row;
    MYSQL_RES      *res;

    SPRAWDZ_TYP(mianownik, "slownik_pobierz");

    mia = mianownik->u.string;

    SPRAWDZ_DLUGOSC(mia, "slownik_pobierz");

    conn = polacz_slownik();

    SPRAWDZ_SLOWNIK(conn);

    (void)sprintf(query,
     "SELECT `mianownik`, `dopelniacz`, `celownik`, `biernik`, `narzednik`, `miejscownik`, `mn_mianownik`, "
             "`mn_dopelniacz`, `mn_celownik`, `mn_biernik`, `mn_narzednik`, `mn_miejscownik`, `rodzaj` FROM `slownik` "
             "WHERE `plain`='%s' OR `mianownik`='%s' OR `mn_mianownik`='%s'", mia, mia, mia);

    mysql_real_query(conn, query, strlen(query));

    res = mysql_use_result(conn);

    SLOWA_NIE_MA_W_SLOWNIKU(res, mia, res);

    row = mysql_fetch_row(res);

    SLOWA_NIE_MA_W_SLOWNIKU(row, mia, res);

    //Sprawdzamy czy pobrali�my w liczbie pojedynczej czy w mnogiej.
    if(row[6] == NULL || (strcmp(mia, row[6]) == 0 && strcmp(mia, row[0]) != 0))
    {
        vec = allocate_array(2);

        vec->item[0].type       = T_POINTER;
        vec->item[0].u.vec      = allocate_array(6);

        for(i = 0, j = (row[6] == NULL ? 0 : 6) ; i < 6 ; i++, j++)
        {
            SLOWO_BLEDNIE_W_SLOWNIKU(row[j], mia, res);

            vec->item[0].u.vec->item[i].type            = T_STRING;
            vec->item[0].u.vec->item[i].string_type     = STRING_MSTRING;
            vec->item[0].u.vec->item[i].u.string        = make_mstring((char *)row[j]);
        }

        vec->item[1].type       = T_NUMBER;
        vec->item[1].u.number   = atoi(row[12]);
    }
    else
    {
        vec = allocate_array(3);

        vec->item[0].type       = T_POINTER;
        vec->item[0].u.vec      = allocate_array(6);

        for(i = 0, j = 0 ; i < 6 ; i++, j++)
        {
            SLOWO_BLEDNIE_W_SLOWNIKU(row[j], mia, res);

            vec->item[0].u.vec->item[i].type            = T_STRING;
            vec->item[0].u.vec->item[i].string_type     = STRING_MSTRING;
            vec->item[0].u.vec->item[i].u.string        = make_mstring((char *)row[j]);
        }

        vec->item[1].type       = T_POINTER;
        vec->item[1].u.vec      = allocate_array(6);

        for(i = 0, j = 6 ; i < 6 ; i++, j++)
        {
            SLOWO_BLEDNIE_W_SLOWNIKU(row[j], mia, res);

            vec->item[1].u.vec->item[i].type            = T_STRING;
            vec->item[1].u.vec->item[i].string_type     = STRING_MSTRING;
            vec->item[1].u.vec->item[i].u.string        = make_mstring((char *)row[j]);
        }

        vec->item[2].type       = T_NUMBER;
        vec->item[2].u.number   = atoi(row[12]);
    }

    mysql_free_result(res);

    rozlacz_slownik(conn);

    return vec;
}

int
        slownik_sprawdz(struct svalue *mianownik)
{
    char       *mia,
    query[174];
    MYSQL      *conn;
    MYSQL_RES  *res;
    MYSQL_ROW   row;

    SPRAWDZ_TYP(mianownik, "slownik_sprawdz");

    mia = mianownik->u.string;

    SPRAWDZ_DLUGOSC(mia, "slownik_sprawdz");

    conn = polacz_slownik();

    SPRAWDZ_SLOWNIK(conn);

    (void)sprintf(query,  "SELECT `id` FROM `slownik` WHERE `plain`='%s' OR `mianownik`='%s' OR `mn_mianownik`='%s'",
     mia, mia, mia);

    mysql_real_query(conn, query, strlen(query));

    res = mysql_use_result(conn);

    if(!res)
    {
        mysql_free_result(res);
        rozlacz_slownik(conn);
        return 0;
    }

    row = mysql_fetch_row(res);

    if(!row)
    {
        mysql_free_result(res);
        rozlacz_slownik(conn);
        return 0;
    }

    mysql_free_result(res);

    rozlacz_slownik(conn);

    return 1;
}
