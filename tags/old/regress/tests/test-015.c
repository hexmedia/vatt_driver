#pragma strict_types

void
create()
{
    if (min(1, 2) != 1)
        throw("min() does not work with integers!\n");

    if (min("a", "b") != "a")
        throw("min() does not work with strings!\n");

    if (min(1.0, 2.0) > 1.1)
        throw("min() does not work with flloats!\n");

    if (min(({1, 2})) != 2)
        throw("min() does not work with integer arrays!\n");

    if (min(({"a", "b"}) != "b")
        throw("min() does not work with string arrays!\n");

    if (min(({1.0, 2.0})) < 1.9)
        throw("min() does not work with float arrays!\n");

    if (max(1, 2) != 2)
        throw("max() does not work with integers!\n");

    if (max("a", "b") != "b")
        throw("max() does not work with strings!\n");

    if (max(1.0, 2.0) < 1.9)
        throw("max() does not work with floats!\n");

    if (max(({1, 2})) != 2)
        throw("max() does not work with integer arrays!\n");

    if (max(({"a", "b"}) != "b")
        throw("max() does not work with string arrays!\n");

    if (max(({1.0, 2.0})) < 1.9)
        throw("max() does not work with float arrays!\n");
}
