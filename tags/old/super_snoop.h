#ifdef SUPER_SNOOP
void read_snoop_file(void);
void update_snoop_file(void);
void check_supersnoop(struct object *);

/**
 * MUD_SNOOPS_DIR
 *
 * Katalog gdzie l�duj� wszystkie snoopy
 */
#define MUD_SNOOPS_DIR      "/vattghern/snoops/"
#define MUD_SNOOPS_SNOOPED  "/vattghern/snoops/.snooped"

#endif
