/**
 * Plik ten jest cz�ci� silnika muda Vatt'ghern(vattghern.pl).
 *
 * (c) 2008 Krystian "Krun" Kuczek  (krun@vattghern.pl)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * and exceptions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the authors may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 4. The code can not be used by Gary Random, Random Communications, Inc.,
 *    the employees of Random Communications, Inc. or its subsidiaries,
 *    including Defiance MUD, without prior written permission from the
 *    authors.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * \file mysql.c
 *
 * Plik ten odpowiedzialny jest za dzia�anie mysql w driverze
 *
 * @author  Krun
 * @date    Pa�dziernik/Listopad 2007
 * @version 2.0
 *
 * TODO:
 *  - defautl id jest do przepisania, bo w chwili obecnej je�li b�dziemy miec mniej wi�cej
 *    w tym samym czasie dwa po��czenie z r�nych miejsc mo�e si� wysypa�.
 */
#include <string.h>
#include "config.h"

#ifdef MYSQL_ENABLED

#include <mysql/mysql.h>
#include <mysql/errmsg.h>

#include "object.h"
#include "lint.h"
#include "mstring.h"
#include "interpret.h"
#include "exec.h"
#include "mudstat.h"
#include "main.h"
#include "simulate.h"
#include "backend.h"
#include "mysql.h"
#include "mysqlw.h"

struct lmysqlp     *lpc_mysql_conn[MYSQL_MAX_CONNECTIONS];

int                 max_id = 1,
                    default_id;

int lpc_mysql_data_type(int typ)
{
    if(typ == MYSQL_TYPE_TINY        ||
       typ == MYSQL_TYPE_SHORT       ||
       typ == MYSQL_TYPE_INT24       ||
       typ == MYSQL_TYPE_LONG        ||
       typ == MYSQL_TYPE_DECIMAL     || //FIXME:CHECK
       typ == MYSQL_TYPE_NEWDECIMAL  || //FIXME:CHECK
       typ == MYSQL_TYPE_ENUM        || //FIXME:CHECK
       typ == MYSQL_TYPE_BIT)           //FIXME:CHECK
    {   //Mamy itegera
        return MYSQL_TYPE_IS_INT;
    }
    else if(typ == MYSQL_TYPE_STRING        ||
            typ == MYSQL_TYPE_TIME          ||
            typ == MYSQL_TYPE_DATE          ||
            typ == MYSQL_TYPE_DATETIME      ||
            typ == MYSQL_TYPE_TIMESTAMP     ||
            typ == MYSQL_TYPE_VAR_STRING    ||
            typ == MYSQL_TYPE_BLOB)
    {   //Mamy stringa
        return MYSQL_TYPE_IS_STRING;
    }
    else if(typ == MYSQL_TYPE_FLOAT      ||
            typ == MYSQL_TYPE_DOUBLE)
    {   //Mamy floata
        return MYSQL_TYPE_IS_FLOAT;
    }
    else
        return 0;
}

static void nie_znaleziono_polaczenia(int id)
{
    error("Nie uda�o si� znale�� po��czenia o id '%d'.\n", id);
}

/**
 * Sprawdzamy czy mysql jest po��czone.
 */
int mysql_is_connected(int id)
{
    if(!id)
        id = default_id;

    for(int i = 0 ; i < MYSQL_MAX_CONNECTIONS ; i++)
        if(lpc_mysql_conn[i]->lid_pol  == id)
            return 1;

    return 0;
}

/**
 * Wyszukujemy indeks po�aczenia.
 */
int wyszukaj_ind_polaczenia(int id)
{
    if(id < 1)
        error("B��dny identyfikator po��czenia mysql.");

    for(int i = 0 ; i < MYSQL_MAX_CONNECTIONS ; i++)
        if(lpc_mysql_conn[i] && lpc_mysql_conn[i]->lid_pol == id)
            return i;

    error("Identyfikator po��czenia mysql '%d' nie istnieje.\n", id);
    return -1;
}

/**
 * Wyszukujemy po��czenie po mudlibowskim id.
 */
MYSQL *wyszukaj_polaczenie(int id)
{
    int i = wyszukaj_ind_polaczenia(id);

    if(i > 0)
        return lpc_mysql_conn[i]->mid_pol;

    return NULL;
}

/**
 * ��czymy si� z baz� danych.
 */
int lpc_mysql_connect(char *serwer, char *uzytkownik, char *haslo, char *baza, int port)
{
    MYSQL *m_l;

    m_l = mysql_init(NULL);

    if(!m_l)
        error("Nie uda�o si� zaincjowa� po��czenia z baz� danych.\n");

    if(!port)
        port = MYSQL_DEFAULT_PORT;

    m_l = mysql_real_connect(m_l, serwer, uzytkownik, haslo, baza, port, NULL, 0);

    if(!m_l)
        error("Nie uda�o si� po��czy� z baz� danych.\n");

    for(unsigned short i = 0 ; i < MYSQL_MAX_CONNECTIONS ; i++)
    {
        if(!lpc_mysql_conn[i])
        {
            lpc_mysql_conn[i] = xalloc(sizeof(struct lmysqlp));
            lpc_mysql_conn[i]->lid_pol = (max_id++);
            lpc_mysql_conn[i]->mid_pol = m_l;
            lpc_mysql_conn[i]->rez     = NULL;
            break;
        }
    }

    default_id = max_id - 1;

    return default_id;
}

/**
 * Zaka�czamy po��czenie z baz� danych.
 */
void lpc_mysql_close(int id)
{
    int i;

    if(!id)
        id = default_id;

    i = wyszukaj_ind_polaczenia(id);

    if(!lpc_mysql_conn[i])
        nie_znaleziono_polaczenia(id);

    if(mysql_is_connected(id))
    {
        mysql_close(lpc_mysql_conn[i]->mid_pol);
        free(lpc_mysql_conn[i]);
    }
}

/**
 * Wy�wietla b��d mysql.
 */
char *lpc_mysql_error(int id)
{
    const char *blad;
    MYSQL *m_l;

    if(!id)
        id = default_id;

    m_l = wyszukaj_polaczenie(id);

    if(!m_l)
        nie_znaleziono_polaczenia(id);

    blad = mysql_error(m_l);

    if(blad == "")
        return 0;

    return (char *)blad;
}

/**
 * Wy�wietla numer b��du.
 */
unsigned int lpc_mysql_errno(int id)
{
    MYSQL *m_l;

    if(!id)
        id = default_id;

    m_l = wyszukaj_polaczenie(id);

    if(!m_l)
        nie_znaleziono_polaczenia(id);

    return (unsigned int)mysql_errno(m_l);
}

/**
 * Czy�cimy ostatnio uzyskane wyniki.
 */
void lpc_mysql_clear_result(int id)
{
    int i;

    if(!id)
        id = default_id;

    i = wyszukaj_ind_polaczenia(id);

    if(!lpc_mysql_conn[i] || !lpc_mysql_conn[i]->mid_pol)
        nie_znaleziono_polaczenia(id);

    if(lpc_mysql_conn[i]->rez)
    {
        mysql_free_result(lpc_mysql_conn[i]->rez);
        lpc_mysql_conn[i]->rez = NULL;
    }
}

/**
 * Wysy�a niebuforowane zapytanie do bazy danych mysql.
 */
int lpc_mysql_uquery(char *query, int id)
{
    MYSQL *m_l;

    if(!id)
        id = default_id;

    m_l = wyszukaj_polaczenie(id);

    if(!m_l)
        nie_znaleziono_polaczenia(id);

    lpc_mysql_clear_result(id);

    int wynik = mysql_real_query(m_l, query, strlen(query));

    if(wynik)
    {
        switch(wynik)
        {
            case CR_COMMANDS_OUT_OF_SYNC:
                error("Niew�a�ciwa komenda do wykonania.");
                break;
            case CR_SERVER_GONE_ERROR:
                error("Serwer znikno� z pola widzenia.\n");
                break;
            case CR_SERVER_LOST:
                error("Stracono po��czenie z serwerem.\n");
                break;
            case CR_UNKNOWN_ERROR:
                error("Nieznany b��d podczas wywo�ania.\n");
                break;
        }
        return 0;
    }

    return 1;
}

/**
 * Wysy�a niebuforowane zapytanie do bazy danych mysql.
 */
int lpc_mysql_query(char *query, int id)
{
    int i;

    if(!id)
        id = default_id;

    i = wyszukaj_ind_polaczenia(id);

    if(!lpc_mysql_conn[i]->mid_pol)
        nie_znaleziono_polaczenia(id);

    lpc_mysql_clear_result(id);

    int wynik = mysql_real_query(lpc_mysql_conn[i]->mid_pol, query, strlen(query));

    if(wynik)
    {
        switch(wynik)
        {
            case CR_COMMANDS_OUT_OF_SYNC:
                error("Niew�a�ciwa komenda do wykonania.");
                break;
            case CR_SERVER_GONE_ERROR:
                error("Serwer znikno� z pola widzenia.\n");
                break;
            case CR_SERVER_LOST:
                error("Stracono po��czenie z serwerem.\n");
                break;
            case CR_UNKNOWN_ERROR:
                error("Nieznany b��d podczas wywo�ania.\n");
                break;
        }
        return 0;
    }

    //Poniewa� zapytanie jest buforowane to musimy doda� wynik.
#ifdef MYSQL_USE_RESULT
    lpc_mysql_conn[i]->rez = mysql_use_result(lpc_mysql_conn[i]->mid_pol);
#else
    lpc_mysql_conn[i]->rez = mysql_store_result(lpc_mysql_conn[i]->mid_pol);
#endif

    if(!lpc_mysql_conn[i]->rez)
        error("Nie uda�o si� pobra� rezultatu.\n");

    return 1;
}

/**
 * Wybieramy baze danych
 */
int lpc_mysql_select_db(char *sel, int id)
{
    MYSQL *m_l;

    if(!id)
        id = default_id;

    m_l = wyszukaj_polaczenie(id);

    if(!m_l)
        nie_znaleziono_polaczenia(id);

    int wynik = mysql_select_db(m_l, sel);

    if(wynik)
    {
        error("Nie uda�o si� wybra� bazy o nazwie %s.\n", sel);
        return 0;
    }

    return 1;
}

/**
 * Zwraca statystyki.
 */
char *lpc_mysql_stat(int id)
{
    const char *stat;
    MYSQL *m_l;

    if(!id)
        id = default_id;

    m_l = wyszukaj_polaczenie(id);

    if(!m_l)
        nie_znaleziono_polaczenia(id);

    stat = mysql_stat(m_l);

    if(stat)
        return (char *)stat;
    else
        return NULL;
}

/**
 * Pingujemy mysql.
 */
int lpc_mysql_ping(int id)
{
    MYSQL *m_l;

    if(!id)
        id = default_id;

    m_l = wyszukaj_polaczenie(id);

    if(!m_l)
        nie_znaleziono_polaczenia(id);

    return (int)mysql_ping(m_l);
}

/**
 * Pobieramy rezultat.
 */
struct svalue *lpc_mysql_fetch_res(MYSQL_RES *rez, int type)
{
    int             num_fields,
                    field_types[MYSQL_MAX_CONNECTIONS];
    MYSQL_ROW       row;
    struct vector  *fields_names,
                   *cur_row;
    struct svalue  *ret;

    if(!rez || rez == NULL)
        error("Nie podano rezultatu, nie mog� pobra� danych.\n");

    if(!type)
        type = MYSQL_NUM;

    num_fields = mysql_num_fields(rez);

    if(num_fields > MYSQL_MAX_FIELDS)
    {
        error("Pobrana ilo�� p�l przekracza maksymaln� dost�pn� ilo�� p�l. Je�li jest ci "
              "konieczna wi�ksza ilo�� pogadaj z kim� z administracji aby zwi�kszy� limit.\n");
    }

    if(!(row = mysql_fetch_row(rez)))
        return NULL;

    fields_names    = allocate_array(num_fields);

    //Wynik musimy zwr�ci� w odpowiedniej formie.
    mysql_field_seek(rez, 0);
    {
        unsigned short i = 0;
        MYSQL_FIELD    *field;

        if(type == MYSQL_ASSOC)
        {
            field = mysql_fetch_field(rez);
            while(field)
            {
                fields_names->item[i].type = T_STRING;
                fields_names->item[i].string_type = STRING_MSTRING;
                fields_names->item[i].u.string = (char *)field->name;
                field_types[i++] = (int)field->type;
                field = mysql_fetch_field(rez);
            }
        }
        else
        {
            field = mysql_fetch_field(rez);
            while(field)
            {
                field_types[i++] = (int)field->type;
                field = mysql_fetch_field(rez);
            }
        }
    }

    cur_row = allocate_array(num_fields);
    for(unsigned int i = 0; i < num_fields; i++)
    {
        if(lpc_mysql_data_type(field_types[i]) == MYSQL_TYPE_IS_INT)
        {   //Mamy int
            //Dodajemy dane do tablicy z kt�rej je przypiszemy do mappinga
            cur_row->item[i].type = T_NUMBER;
            cur_row->item[i].u.number = atoi(row[i]);
        }
        else if(lpc_mysql_data_type(field_types[i]) == MYSQL_TYPE_IS_STRING)
        {   //Mamy string.
                //Dodajemy dane do tablicy z kt�rej je przypiszemy do mappinga
            cur_row->item[i].type = T_STRING;
            cur_row->item[i].string_type = STRING_MSTRING;
            cur_row->item[i].u.string = make_mstring((char*)row[i]);
        }
        else if(lpc_mysql_data_type(field_types[i]) == MYSQL_TYPE_IS_FLOAT)
        {   //Mamy floata
            //Dodajemy dane do tablicy z kt�rej je przypiszemy do mappinga
            cur_row->item[i].type = T_FLOAT;
            cur_row->item[i].u.real = strtod(row[i], NULL);
        }
        else
        {   //Niezdefiniowany typ.
            //Mo�e si� zdarzy�, �e jaki� typ przez moje niedopatrzenie nie zosta� wprowadzony.
            error("Nieobs�ugiwany typ danych pobrany z '%d' pola - pobrany typ to '%d'.\n", i, field_types[i]);
            return 0;
        }
    }

    if(type == MYSQL_ASSOC)
    {
        ret = xalloc(sizeof(struct svalue));

        ret->type   = T_MAPPING;
        ret->u.map  = make_mapping(fields_names, cur_row);

        return ret;
    }
    else if(type == MYSQL_NUM)
    {
        ret = xalloc(sizeof(struct svalue));

        ret->type   = T_POINTER;
        ret->u.vec  = cur_row;

        return ret;
    }
    else
        return NULL;
}

/**
 * Pobieramy rezultat ostatniego zapytania
 */
MYSQL_RES *ostatni_rezultat(int id)
{
    int i;

    if(!id)
        id = default_id;

    i = wyszukaj_ind_polaczenia(id);

    if(!lpc_mysql_conn[i]->mid_pol)
        nie_znaleziono_polaczenia(id);

    if(!lpc_mysql_conn[i]->rez)
        return NULL;

    return lpc_mysql_conn[i]->rez;
}

/**
 * Pobieramy kolumne indeksowan� numerami.
 */
struct svalue *lpc_mysql_fetch_row(int id)
{
    MYSQL_RES *lr;

    lr = ostatni_rezultat(id);

    return lpc_mysql_fetch_res(lr, MYSQL_NUM);
}

/**
 * Pobieramy kolumne indeksowan� nazwami p�l w tabeli mysql.
 */
struct svalue *lpc_mysql_fetch_assoc(int id)
{
    MYSQL_RES *lr;

    lr = ostatni_rezultat(id);

    return lpc_mysql_fetch_res(lr, MYSQL_ASSOC);
}

#endif
