/**
 * Plik ten jest cz�ci� silnika muda Vatt'ghern(vattghern.pl).
 *
 * (c) 2008 Krystian "Krun" Kuczek  (krun@vattghern.pl)
 * All rights reserved (r).
 *
 * Niekt�re funkcje zosta�y przeniesione z innych plik�w
 * aby wprowadzi� troch� wi�kszy porz�dek w mudlibie.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * and exceptions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the authors may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 4. The code can not be used by Gary Random, Random Communications, Inc.,
 *    the employees of Random Communications, Inc. or its subsidiaries,
 *    including Defiance MUD, without prior written permission from the
 *    authors.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>

#include "config.h"
#include "lint.h"
#include "mstring.h"
#include "simulate.h"
#include "interpret.h"

double current_time;

/*
 * The function time() can't really be trusted to return an integer.
 * But this game uses the 'current_time', which is an integer number
 * of seconds. To make this more portable, the following functions
 * should be defined in such a way as to retrun the number of seconds since
 * some chosen year. The old behaviour of time(), is to return the number
 * of seconds since 1970.
 *
 * alarm_time must never move backwards.
 */
void
set_current_time()
{
    static double alarm_base_time, alarm_last_time;
    struct timeval tv;

    (void)gettimeofday(&tv, NULL);
    current_time = tv.tv_sec + tv.tv_usec * 1e-6;

    if (alarm_base_time == 0)
        alarm_base_time = current_time;

    alarm_time = current_time - alarm_base_time;

    if (alarm_time < alarm_last_time)
    {
        alarm_time = alarm_last_time;
        alarm_base_time = current_time - alarm_time;
    }

    alarm_last_time = alarm_time;
}

/**
 * @return
 * <ul>
 *   <li> <i>1</i> - rok jest przest�pny</li>
 *   <li> <i>0</i> - rok nie jest przest�pny</li>
 * </ul>
 */
int czy_przestepny(unsigned int rok)
{
    return ((rok % 4 == 0) && ((rok % 100 != 0) || (rok % 400 == 0)));
}

/**
 * @return D�ugo�� miesi�ca.
 */
int month_length(unsigned int miesiac, unsigned int rok)
{
    switch(miesiac)
    {
        case 1:     return 31;
        case 2:     return (czy_przestepny(rok) ? 29 : 28);
        case 3:     return 31;
        case 4:     return 30;
        case 5:     return 31;
        case 6:     return 30;
        case 7:     return 31;
        case 8:     return 31;
        case 9:     return 30;
        case 10:    return 31;
        case 11:    return 30;
        case 12:    return 31;
    }

    return 1;
}

/**
 * @return Skr�cona nazwa dnia tygodnia.
 */
char * date_return_D(int wd)
{
    switch(wd)
    {
        case 0: return "Pon";
        case 1: return "Wt";
        case 2: return "�r";
        case 3: return "Czw";
        case 4: return "Pt";
        case 5: return "Sob";
        case 6: return "Nie";
    }

    return NULL;
}

/**
 * @return nazwa dnia tygodnia
 */
char *date_return_l(int wd)
{
    switch(wd)
    {
        case 0: return "Poniedzia�ek";
        case 1: return "Wtorek";
        case 2: return "�roda";
        case 3: return "Czwartek";
        case 4: return "Pi�tek";
        case 5: return "Sobota";
        case 6: return "Niedziela";
    }

    return NULL;
}

char *date_return_F(int month)
{
    switch(month)
    {
        case 0:     return "Stycze�";
        case 1:     return "Luty";
        case 2:     return "Marzec";
        case 3:     return "Kwiecie�";
        case 4:     return "Maj";
        case 5:     return "Czerwiec";
        case 6:     return "Lipiec";
        case 7:     return "Sierpie�";
        case 8:     return "Wrzesie�";
        case 9:     return "Pa�dziernik";
        case 10:    return "Listopad";
        case 11:    return "Grudzie�";
    }

    return NULL;
}

char *date_return_M(int miesiac)
{
    switch(miesiac)
    {
        case 0:     return "Sty";
        case 1:     return "Lut";
        case 2:     return "Mar";
        case 3:     return "Kwi";
        case 4:     return "Maj";
        case 5:     return "Czerw";
        case 6:     return "Lip";
        case 7:     return "Sie";
        case 8:     return "Wrz";
        case 9:     return "Pa�";
        case 10:    return "Lis";
        case 11:    return "Gru";
    }

    return NULL;
}

char *date_return_R(int miesiac)
{
    switch(miesiac)
    {
        case 0:     return "I";
        case 1:     return "II";
        case 2:     return "III";
        case 3:     return "IV";
        case 4:     return "V";
        case 5:     return "VI";
        case 6:     return "VII";
        case 7:     return "VIII";
        case 8:     return "IX";
        case 9:     return "X";
        case 10:    return "XI";
        case 11:    return "XII";
    }

    return NULL;
}

/**
 * Funkcja zwraca stringa z reprezentacj� podanego czasu w podanym formacie.
 *
 * @param tim <i>time_t</i> timestamp
 * @param format <i>char *</i> format
 *
 */
char *
time_string(time_t tim, char *format)
{
    int flen, i;
    char bufor[30], tret[2000], *ret;
    struct tm *tm;
    flen = strlen(format);

    if(!flen)
        return NULL;
    else if(flen > 500)
    {
        error("Maksymalny rozmiar formatu do date to 500 znak�w.\n");
        return NULL;
    }

    tm = localtime((time_t *)&tim);

    ret = NULL;
    (void)strcpy(tret, "");

    for(i = 0 ; i < flen ; i++)
    {
        switch(format[i])
        {
            //DZIE�:
            case 'd':   (void)sprintf(bufor, "%02d", tm->tm_mday);                      break;
            case 'D':   (void)strcpy(bufor, date_return_D(tm->tm_wday));                break;
            case 'j':   (void)sprintf(bufor, "%d", tm->tm_mday);                        break;
            case 'l':   (void)strcpy(bufor, date_return_l(tm->tm_wday));                break;
            case 'N':   (void)sprintf(bufor, "%d", (tm->tm_wday + 1));                  break;
            case 'w':   (void)sprintf(bufor, "%d", tm->tm_wday);                        break;
            case 'z':   (void)sprintf(bufor, "%d", tm->tm_yday);                        break;
            case 'Z':   (void)sprintf(bufor, "%d", (tm->tm_yday + 1));                  break;

            //Miesi�c:
            case 'F':   (void)strcpy(bufor, date_return_F(tm->tm_mon));                 break;
            case 'm':   (void)sprintf(bufor, "%02d", (tm->tm_mon + 1));                 break;
            case 'M':   (void)strcpy(bufor, date_return_M(tm->tm_mon));                 break;
            case 'n':   (void)sprintf(bufor, "%d", (tm->tm_mon + 1));                   break;
            case 'R':   (void)strcpy(bufor, date_return_R(tm->tm_mon));                 break;
            case 't':   (void)sprintf(bufor, "%d",
                            month_length((tm->tm_mon + 1), (tm->tm_year + 1900)));      break;

            //Rok:
            case 'Y':   (void)sprintf(bufor, "%d", (tm->tm_year + 1900));               break;
            case 'y':   (void)sprintf(bufor, "%02d",
                            (tm->tm_year > 100 ? tm->tm_year - 100 : tm->tm_year));     break;

            //Godzina:
            case 'h':   (void)sprintf(bufor, "%02d", (tm->tm_hour % 12));               break;
            case 'H':   (void)sprintf(bufor, "%02d", tm->tm_hour);                      break;
            case 'g':   (void)sprintf(bufor, "%d", (tm->tm_hour % 12));                 break;
            case 'G':   (void)sprintf(bufor, "%d", tm->tm_hour);                        break;

            //Minuta:
            case 'i':   (void)sprintf(bufor, "%02d", tm->tm_min);                       break;
            case 'I':   (void)sprintf(bufor, "%d", tm->tm_min);                         break;

            //Sekunda:
            case 's':   (void)sprintf(bufor, "%02d", tm->tm_sec);                       break;
            case 'S':   (void)sprintf(bufor, "%d", tm->tm_sec);                         break;

            case 'u':   (void)sprintf(bufor, "%d",
                            (int)(100.0 * (current_time - (time_t)current_time)));      break;

            case '\\':  if(i < flen) i++;   //Specjalnie brak breaka

            default:
                bufor[0] = format[i];
                bufor[1] = '\0';
        }
        (void)strcat(tret, bufor);
    }

    if(!strlen(tret))
        return NULL;
    else
    {
        ret = make_mstring(tret);
        return ret;
    }
}

/**
 * Funkcja zwraca string zawieraj�cy aktualn� date w formacie:
 * Czw,  1 IV 2008 22:48:02, w formacie zwartym nie wyst�puj�
 * podw�jne spacje.
 *
 * @param t <i>time_t</i> - timestamp
 * @param zw <i>int</i> - czy format jest zwarty.
 */
char *ctime_string(time_t t, int zw)
{
    if(zw != 0)
        return time_string(t, "D, j R Y G:i:s");
    else
    {
        int i, length;
        char *dzient, *dzienm, *miesiac, *rok, *godzina, *minuta, *sekunda, *ret;

        dzient      = time_string(t, "D");
        dzienm      = time_string(t, "j");
        miesiac     = time_string(t, "R");
        rok         = time_string(t, "Y");
        godzina     = time_string(t, "G");
        minuta      = time_string(t, "i");
        sekunda     = time_string(t, "s");

        length = 27 + 2;

        ret = allocate_mstring(length);

        (void)strcpy(ret, dzient);

        if(strlen(dzient) == 2 && strlen(dzienm) == 1)
            (void)strcat(ret, ",   ");
        else if(strlen(dzient) == 2 || strlen(dzienm) == 1)
            (void)strcat(ret, ",  ");
        else
            (void)strcat(ret, ", ");

        (void)strcat(ret, dzienm);
        (void)strcat(ret, " ");

        for(i = 0 ; i < (4 - strlen(miesiac)) ; i++)
            (void)strcat(ret, " ");

        (void)strcat(ret, miesiac);
        (void)strcat(ret, " ");
        (void)strcat(ret, rok);
        (void)strcat(ret, ", ");
        (void)strcat(ret, godzina);
        (void)strcat(ret, ":");
        (void)strcat(ret, minuta);
        (void)strcat(ret, ":");
        (void)strcat(ret, sekunda);

        free_mstring(dzient);
        free_mstring(dzienm);
        free_mstring(miesiac);
        free_mstring(rok);
        free_mstring(godzina);
        free_mstring(minuta);
        free_mstring(sekunda);

        return ret;
    }
}

/**
 * @return Tablice z aktualnym czasem
 *  <ul>
 *    <li> <i>0</i> - sekunda</li>
 *    <li> <i>1</i> - minuta</li>
 *    <li> <i>2</i> - godzina</li>
 *    <li> <i>3</i> - dzie� miesi�ca</li>
 *    <li> <i>4</i> - miesi�c</li>
 *    <li> <i>5</i> - rok</li>
 *    <li> <i>6</i> - dzie� tygodnia</li>
 *    <li> <i>7</i> - dzie� roku</li>
 *    <li> <i>8</i> - d�ugo�� bierz�cego miesi�ca</li>
 *    <li> <i>9</i> - czy rok jest przest�pny</li>
 *  </ul>
 */
struct vector *lpc_localtime(time_t t)
{
    struct tm *czas;
    struct vector *vec;

    czas = localtime(&t);

    vec = allocate_array(10);

    vec->item[0].u.number = (long long)czas->tm_sec;
    vec->item[1].u.number = (long long)czas->tm_min;
    vec->item[2].u.number = (long long)czas->tm_hour;
    vec->item[3].u.number = (long long)czas->tm_mday;
    vec->item[4].u.number = (long long)czas->tm_mon + 1;
    vec->item[5].u.number = (long long)czas->tm_year + 1900;
    vec->item[6].u.number = (long long)czas->tm_wday;
    vec->item[7].u.number = (long long)czas->tm_yday + 1;
    vec->item[8].u.number = month_length((czas->tm_mon + 1), (czas->tm_year + 1900));
    vec->item[9].u.number = (short)czy_przestepny(czas->tm_year + 1900);

    return vec;
}

time_t lpc_mktime(int s, int i, int h, int d, int m, int y)
{
    time_t t, ct;
    struct tm *tm;

    ct = (time_t)current_time;
    tm = localtime(&ct);

    if(s != -1)
        tm->tm_sec = s;

    if(i != -1)
        tm->tm_min = i;

    if(h != -1)
        tm->tm_hour = h;

    if(d != -1)
    {
        tm->tm_yday = 0;
        tm->tm_wday = 0;
        tm->tm_mday = d;
    }

    if(m != -1)
        tm->tm_mon = m - 1;

    if(y != -1)
        tm->tm_year = y - 1900;

    t = mktime(tm);

    return t;
}
