/**
 * Plik ten jest cz�ci� silnika muda Vatt'ghern(vattghern.pl).
 *
 * (c) 2008 Krystian "Krun" Kuczek  (krun@vattghern.pl)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * and exceptions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the authors may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 4. The code can not be used by Gary Random, Random Communications, Inc.,
 *    the employees of Random Communications, Inc. or its subsidiaries,
 *    including Defiance MUD, without prior written permission from the
 *    authors.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef MYSQL_ENABLED

/**
 * MYSQL_MAX_FIELDS
 *
 * Maksymalna ilo�� p�l w tabelach mysql. Przy wi�kszej ilo�ci nie b�dzie pobiera�.
 * Na razie ustawiam na 30, zobaczymy czy trzeba b�dzie wi�cej.
 */
#define MYSQL_MAX_FIELDS        30

/**
 * MYSQL_MAX_CONENCTIONS
 *
 * Definiuje ile mo�na maksymalnie wykona� do mysql'a.
 */
#define MYSQL_MAX_CONNECTIONS   MYSQL_ENABLED

/**
 * MYSQL_USE_RESULT
 *
 * Spos�b pobierania wynik�w
 */
#define MYSQL_USE_RESULT

/**
 * MYSQL_DEFAULT_PORT
 *
 * Domy�lny port mysql.
 */
#define MYSQL_DEFAULT_PORT      3306

#define MYSQL_TYPE_IS_INT       1
#define MYSQL_TYPE_IS_FLOAT     2
#define MYSQL_TYPE_IS_STRING    3

#define MYSQL_NUM               0
#define MYSQL_ASSOC             1

#define MYSQL_DISABLED(n)       (pop_n_elems(n);push_number(0);)

extern int              lpc_mysql_connect(char *, char *, char *, char *, int);
extern void             lpc_mysql_close(int);
extern char            *lpc_mysql_error(int);
extern unsigned int     lpc_mysql_errno(int);
extern int              lpc_mysql_uquery(char *, int);
extern int              lpc_mysql_query(char *, int);
extern int              lpc_mysql_select_db(char *, int);
extern void             lpc_mysql_clear_result(int);
extern char            *lpc_mysql_stat(int);
extern int              lpc_mysql_ping(int);
extern struct svalue   *lpc_mysql_fetch_row(int);
extern struct svalue   *lpc_mysql_fetch_array(int);
extern struct svalue   *lpc_mysql_fetch_assoc(int);

#endif
